<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Core;

use BullitEngine\Component\Container\Container;
use BullitEngine\Component\Container\Exception\ContainerException;
use BullitEngine\Component\Container\Service;
use BullitEngine\Component\Http\Request;
use BullitEngine\Component\Http\Response;
use BullitEngine\Component\Routing\Exception\ResourceNotFoundException;
use BullitEngine\Component\Routing\Router;

/**
 * Class Application
 *
 * @package BullitEngine\Component\Core
 */
class Application extends Container
{
    /** @var  Router */
    protected $router;

    /**
     * @param mixed $parameters
     */
    public function __construct($parameters = [])
    {
        if (is_subclass_of($parameters, 'BullitEngine\\Component\\Config\\ConfigParserInterface')) {
            /** @noinspection PhpUndefinedMethodInspection */
            $parameters = $parameters->getConfig();
        }

        parent::__construct($parameters);
        $this->init();
    }

    /**
     * Initializes application.
     */
    private function init()
    {
        $this->registerRouter();
        $this->registerDatabase();
        $this->registerTemplateCompiler();
        $this->registerViewTypes();

        $this->registerAliases();
    }

    /**
     * Registers a router service.
     *
     * @throws \BullitEngine\Component\Container\Exception\ContainerException
     */
    private function registerRouter()
    {
        try {
            $classname = $this->getParameter('router.class');
        } catch (\RuntimeException $e) {
            $classname = 'BullitEngine\\Component\\Routing\\Router';
        }

        $container = $this;
        $this->register(new Service('router', function() use ($classname, $container) {
            return new $classname($container);
        }));
        $this->addAlias('router', $classname);
    }

    /**
     * Registers a database service.
     */
    private function registerDatabase()
    {
        try {
            $adapter = $this->getParameter('database.adapter');
        } catch (\RuntimeException $e) {
            $adapter = 'BullitEngine\\Component\\Database\\PdoAdapter';
        }

        try {
            $dsn      = $this->getParameter('database.dsn');
            $username = $this->getParameter('database.user');
            $password = $this->getParameter('database.passwd');

            $this->register(new Service('database', function() use ($adapter, $dsn, $username, $password) {
                return new $adapter($dsn, $username, $password);
            }));
            $this->addAlias('database', $adapter);
        } catch (ContainerException $e) {
        }
    }

    /**
     * Register compiler class.
     */
    private function registerTemplateCompiler()
    {
        try {
            $classname = $this->getParameter('template.compiler');
        } catch (\RuntimeException $e) {
            $classname = 'BullitEngine\\Component\\View\\Compiler';
        }

        try {
            $cache_path = $this->getParameter('template.cache');
        } catch (\RuntimeException $e) {
            $cache_path = getcwd().DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'cache';
        }

        $this->register(new Service('template-compiler', function() use ($classname, $cache_path) {
            return new $classname($cache_path);
        }));
    }

    /**
     * Registers view types.
     */
    private function registerViewTypes()
    {
        try {
            $classname = $this->getParameter('template.class');
        } catch (\RuntimeException $e) {
            $classname = 'BullitEngine\\Component\\View\\TemplateView';
        }

        $this->addAlias('TemplateView', $classname);

        $compiler = $this->service('template-compiler');
        $service  = new Service('TemplateView', function() use ($classname, $compiler) {
            return new $classname($compiler);
        });
        $service->setIsShared(false);

        $this->register($service);
    }

    /**
     * Register core aliases.
     */
    private function registerAliases()
    {
        $this->addAlias('database', 'BullitEngine\Component\Database\DatabaseAdapterInterface');
    }

    /**
     * Handle an incomming request.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function handle(Request $request) : Response
    {
        try {
            $response = $this->service('router')
                             ->dispatch($request);
        } catch (ResourceNotFoundException $e) {
            $response = new Response('Resource not found.', 404);
        } catch (\Exception $e) {
            $response = new Response('An error occurred.', 500);
        }

        return $response;
    }
}

