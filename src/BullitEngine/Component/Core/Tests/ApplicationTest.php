<?php

namespace BullitEngine\Component\Core\Tests;

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use BullitEngine\Component\Core\Application;
use BullitEngine\Component\Http\Request;
use BullitEngine\Component\Http\Response;
use BullitEngine\Component\Routing\Route;
use BullitEngine\Component\Routing\Router;
use BullitEngine\Component\View\Compiler;
use BullitEngine\Component\View\TemplateView;
use PHPUnit\Framework\TestCase;

class ApplicationTest extends TestCase
{
    public function testConstructorArrayAsParameters()
    {
        $config = [ 'foo' => 'bar' ];
        $app    = new Application($config);
        $this->assertEquals('bar', $app->getParameter('foo'));
    }

    public function testConstructorConfigParserAsParameters()
    {
        $config = $this->getMockBuilder('BullitEngine\\Component\\Config\\ConfigParserJson')
                       ->disableOriginalConstructor()
                       ->getMock();

        $config->expects($this->any())
               ->method('getConfig')
               ->willReturn([ 'foo' => 'bar' ]);

        /** @noinspection PhpParamsInspection */
        $app = new Application($config);
        $this->assertEquals('bar', $app->getParameter('foo'));
    }

    public function testRegisterDatabase()
    {
        $config = [
            'database.dsn'    => $GLOBALS['DB_DSN'],
            'database.user'   => $GLOBALS['DB_USER'],
            'database.passwd' => $GLOBALS['DB_PASSWD']
        ];

        $app = new Application($config);
        $this->assertInstanceOf('BullitEngine\\Component\\Database\\DatabaseAdapterInterface', $app->service('database'));
    }

    public function testHandleOk()
    {
        $server  = [
            'REQUEST_URI'    => '/foo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo',
            'REQUEST_METHOD' => 'GET'
        ];
        $request = new Request([], [], [], [], $server);

        $app    = new Application();
        $router = $app->service('router');
        $router->add('foo', new Route('/foo', function() {
            return new Response('OK');
        }));

        $this->assertEquals(new Response('OK'), $app->handle($request));
    }

    public function testHandle404()
    {
        $server  = [
            'REQUEST_URI'    => '/bar',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/bar',
            'REQUEST_METHOD' => 'GET'
        ];
        $request = new Request([], [], [], [], $server);

        $app    = new Application();
        $router = $app->service('router');
        $router->add('foo', new Route('/foo', function() {
            return new Response('OK');
        }));

        $response = $app->handle($request);
        $this->assertEquals(404, $response->statusCode());
    }

    public function testHandle500()
    {
        $server  = [
            'REQUEST_URI'    => '/foo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo',
            'REQUEST_METHOD' => 'GET'
        ];
        $request = new Request([], [], [], [], $server);

        $app    = new Application();
        $router = $app->service('router');
        $router->add('foo', new Route('/foo', function() {
            throw new \Exception();
        }));

        $response = $app->handle($request);
        $this->assertEquals(500, $response->statusCode());
    }

    public function testDefaultRegistration()
    {
        $app = new Application();
        $this->assertInstanceOf(Router::class, $app->service('router'));
        $this->assertInstanceOf(Compiler::class, $app->service('template-compiler'));
        $this->assertInstanceOf(TemplateView::class, $app->service('TemplateView'));
    }
}
