<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Orm;

/**
 * Class EntityPropertiesIterator
 *
 * @package BullitEngine\Component\Orm
 */
class EntityPropertiesIterator implements \Iterator
{
    /** @var Entity */
    private $entity;

    /** @var array */
    private $values = [];

    /**
     * Constructor.
     *
     * @param Entity $entity
     */
    public function __construct(Entity $entity)
    {
        $this->entity = $entity;

        $entity_columns = $entity->columns();
        $this->values   = array_combine($entity_columns, array_fill(0, count($entity_columns), null));
    }

    /**
     * Return the current element
     *
     * @link http://php.net/manual/en/iterator.current.php
     *
     * @return mixed Can return any type.
     */
    public function current()
    {
        if (null === current($this->values)) {
            $key                = $this->key();
            $this->values[$key] = call_user_func([ $this->entity, $key ]);
        }

        return current($this->values);
    }

    /**
     * Return the key of the current element
     *
     * @link http://php.net/manual/en/iterator.key.php
     *
     * @return string
     */
    public function key() : ?string
    {
        return key($this->values);
    }

    /**
     * Move forward to next element
     *
     * @link http://php.net/manual/en/iterator.next.php
     *
     * @return void Any returned value is ignored.
     */
    public function next() : void
    {
        next($this->values);
    }

    /**
     * Checks if current position is valid
     *
     * @link http://php.net/manual/en/iterator.valid.php
     *
     * @return boolean The return value will be casted to boolean and then evaluated.
     *       Returns true on success or false on failure.
     */
    public function valid() : bool
    {
        return key($this->values) !== null;
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @link http://php.net/manual/en/iterator.rewind.php
     *
     * @return void Any returned value is ignored.
     */
    public function rewind() : void
    {
        reset($this->values);
    }
}

