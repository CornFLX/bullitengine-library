<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Orm\Tests;

use BullitEngine\Component\Orm\Entity;
use PHPUnit\Framework\TestCase;

class EntityTest extends TestCase
{
    public function testConstructor()
    {
        $entity = new MyEntity();
        $this->assertInstanceOf('BullitEngine\\Component\\Orm\\Entity', $entity);
    }

    public function testColumns()
    {
        $entity = new MyEntity();
        $this->assertEquals([ 'id' ], $entity->columns());
    }

    public function testGetIterator()
    {
        $entity = new MyEntity();
        $entity->setId(1);

        $actual = [];
        foreach ($entity as $key => $value) {
            $actual[$key] = $value;
        }

        $this->assertEquals([ 'id' => 1 ], $actual);
    }
}

class MyEntity extends Entity
{
    private $id;
    private $str;
    private $_attr;

    public function id()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function attr()
    {
        return $this->_attr;
    }

    public function setStr($str)
    {
        $this->str = $str;
    }
}
