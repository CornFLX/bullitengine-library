<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Orm\Tests;

use BullitEngine\Component\Orm\Entity;
use BullitEngine\Component\Orm\Mapper;
use PHPUnit\Framework\TestCase;

class MapperTest extends TestCase
{
    private $_datasource;

    public function testConstructor()
    {
        $mapper = new ConcreteMapper($this->_datasource);
        $this->assertInstanceOf('BullitEngine\\Component\\Orm\\Mapper', $mapper);
        $this->assertEquals('concrete', $mapper->table());
        $this->assertEquals('BullitEngine\\Component\\Orm\\Tests\\Concrete', $mapper->entityClass());
    }

    public function testConstructorInvalidClass()
    {
        $this->expectException(\RuntimeException::class);

        $mapper = new ConcreteMapper3($this->_datasource);
        $this->assertInstanceOf('BullitEngine\\Component\\Orm\\Mapper', $mapper);
    }

    public function testGet()
    {
        $mapper = new ConcreteMapper($this->_datasource);
        $entity = $mapper->get(1);
        $this->assertInstanceOf('BullitEngine\\Component\\Orm\\Entity', $entity);
    }

    public function testGetNotExists()
    {
        $mapper = new ConcreteMapper($this->_datasource);
        $this->assertNull($mapper->get(2));
    }

    public function testGetWithCompositeKey()
    {
        $mapper = new ConcreteMapper2($this->_datasource);
        $entity = $mapper->get([ 'id_1' => 1, 'id_2' => 5 ]);
        $this->assertInstanceOf('BullitEngine\\Component\\Orm\\Entity', $entity);
    }

    public function testGetWithIncompleteCompositeKey()
    {
        $this->expectException(\LogicException::class);

        $mapper = new ConcreteMapper2($this->_datasource);
        $mapper->get([ 'id_1' => 1 ]);
    }

    public function testInsert()
    {
        $entity = new Concrete();
        $entity->setStr('bar');

        $another_mapper = new AnotherMapper($this->_datasource);
        $entity->setAnother($another_mapper->get(1));


        $mapper = new ConcreteMapper($this->_datasource);

        $this->assertTrue($mapper->insert($entity));
        $this->assertEquals(2, $entity->id());
    }

    public function testUpdate()
    {
        $mapper = new ConcreteMapper($this->_datasource);
        $entity = $mapper->get(1);
        /** @noinspection PhpUndefinedMethodInspection */
        $entity->setStr('bar');

        $this->assertEquals(1, $mapper->update($entity));
    }

    public function testDelete()
    {
        $mapper = new ConcreteMapper($this->_datasource);
        $entity = $mapper->get(1);
        $this->assertEquals(1, $mapper->delete($entity));
    }

    public function testIdentifier()
    {
        $mapper = new ConcreteMapper($this->_datasource);
        $this->assertEquals([ 'id' ], $mapper->identifier());
    }

    protected function setUp()
    {
        $this->_datasource = $this->getMockBuilder('BullitEngine\\Component\\Database\\PdoAdapter')
                                  ->disableOriginalConstructor()
                                  ->getMock();

        $this->_datasource->expects($this->any())
                          ->method('one')
                          ->willReturnMap([
                              [ "SELECT * FROM `concrete` WHERE `id` = :id", [ 'id' => 1 ], [ 'id' => 1, 'str' => 'foo', 'another' => 1 ] ],
                              [
                                  "SELECT * FROM `concrete2` WHERE `id_1` = :id_1 AND `id_2` = :id_2",
                                  [ 'id_1' => 1, 'id_2' => 5 ],
                                  [ 'id_1' => 1, 'id_2' => 5, 'str' => 'foo' ]
                              ],
                              [ "SELECT * FROM `another` WHERE `id` = :id", [ 'id' => 1 ], [ 'id' => 1 ] ]
                          ]);

        $this->_datasource->expects($this->any())
                          ->method('insert')
                          ->willReturn(true);

        $this->_datasource->expects($this->any())
                          ->method('lastInsertedId')
                          ->willReturn(2);

        $this->_datasource->expects($this->any())
                          ->method('update')
                          ->willReturn(1);

        $this->_datasource->expects($this->any())
                          ->method('delete')
                          ->willReturn(1);
    }
}


class ConcreteMapper extends Mapper
{
}

class Concrete extends Entity
{
    private $id;
    private $str;
    private $another;

    public function id()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function str()
    {
        return $this->str;
    }

    public function setStr($str)
    {
        $this->str = $str;
    }

    public function another()
    {
        return $this->another;
    }

    public function setAnother(Another $another)
    {
        $this->another = $another;
    }


}

class ConcreteMapper2 extends Mapper
{
    protected $entity_class = 'BullitEngine\\Component\\Orm\\Tests\\Concrete2';
    protected $table        = 'concrete2';
    protected $identifier   = [ 'id_1', 'id_2' ];
}

class Concrete2 extends Entity
{
    private $_id_1;
    private $_id_2;
    private $_str;

    public function setId1($id1)
    {
        $this->_id_1 = $id1;
    }

    public function setId2($id2)
    {
        $this->_id_2 = $id2;
    }

    public function setStr($str)
    {
        $this->_str = $str;
    }

}

class ConcreteMapper3 extends Mapper
{
}

class Another extends Entity
{
    private $id;

    public function id()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
}

class AnotherMapper extends Mapper
{
}
