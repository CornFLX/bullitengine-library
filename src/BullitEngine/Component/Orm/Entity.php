<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Orm;

use Traversable;

/**
 * Class Entity
 *
 * @package BullitEngine\Component\Orm
 */
abstract class Entity implements \IteratorAggregate
{
    /** @var array */
    protected $_columns = null;

    /**
     * Constructor.
     */
    final public function __construct()
    {
        $this->init();
    }

    /**
     * Called in constructor. Override this method if you need specific initialisation.
     */
    protected function init() : void
    {
    }

    /**
     * Returns an array with all _columns.
     *
     * @return array
     */
    public function columns() : array
    {
        if (null === $this->_columns) {
            $this->_columns = $this->determineColumns();
        }

        return $this->_columns;
    }

    /**
     * @return array
     */
    private function determineColumns() : array
    {
        $columns = [];

        $reflect    = new \ReflectionClass($this);
        $properties = $reflect->getProperties(\ReflectionProperty::IS_PRIVATE | \ReflectionProperty::IS_PROTECTED);

        foreach ($properties as $property) {
            $name = $property->getName();

            // By convention, if property's name start with a "_", it's not reprensents one column.
            if ($name[0] === '_') {
                continue;
            }

            // By convention again, properties must have a getter named "column()".
            if (!$reflect->hasMethod($name)) {
                continue;
            }

            $columns[] = $name;
        }

        return $columns;
    }

    /**
     * Retrieve an external iterator
     *
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     *
     * @return Traversable An instance of an object implementing Iterator or
     *          Traversable
     */
    public function getIterator() : Traversable
    {
        return new EntityPropertiesIterator($this);
    }
}
