<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Orm;

use BullitEngine\Component\Database\DatabaseAdapterInterface;
use BullitEngine\Component\Database\Exception\QueryBuilderException;
use BullitEngine\Component\Database\QueryBuilder;

/**
 * Class Mapper
 *
 * @package BullitEngine\Component\Orm
 */
abstract class Mapper
{
    /** @var DatabaseAdapterInterface */
    protected $datasource;

    /** @var string */
    protected $table;

    /** @var string */
    protected $entity_class;

    /** @var array */
    protected $identifier = [ 'id' ];

    /**
     * Constructor.
     *
     * @param DatabaseAdapterInterface $datasource
     *
     * @throws \RuntimeException
     */
    public function __construct(DatabaseAdapterInterface $datasource)
    {
        $this->datasource = $datasource;

        $classname = get_called_class();
        $classname = substr($classname, strrpos($classname, '\\') + 1, -6);

        // Compute table property if it is not defined.
        if (null === $this->table) {
            $this->table = strtolower($classname);
        }

        // Compute entity class if it is not defined.
        if (null === $this->entity_class) {
            $this->computeEntityClass();
        }
    }

    /**
     * @throws \RuntimeException
     */
    private function computeEntityClass() : void
    {
        $class = substr(get_called_class(), 0, -6);

        if (class_exists($class)) {
            $this->entity_class = $class;
        } else {
            throw new \RuntimeException('Unable to compute entity class.');
        }
    }

    /**
     * @param string $identifier
     *
     * @return string
     */
    private function computeSetter(string $identifier) : string
    {
        $setter = 'set'.ucwords($identifier, '-_');
        $setter = str_replace([ '-', '_' ], '', $setter);

        return $setter;
    }

    /**
     * Get an entity by its identifier.
     *
     * @param mixed $id
     *
     * @return Entity|null
     * @throws QueryBuilderException
     * @throws \LogicException
     */
    public function get($id) : ?Entity
    {
        $builder = QueryBuilder::factory();
        $builder->select([ '*' ])
                ->from($this->table)
                ->where();

        $bindings = [];
        foreach ($this->identifier as $identifier_part) {
            $builder->andWhere($builder->formatColumn($identifier_part).' = '.$builder->formatBind($identifier_part));

            if (is_array($id) && isset($id[$identifier_part])) {
                $bindings[$identifier_part] = $id[$identifier_part];
            } else {
                if (!is_array($id) && 1 === count($this->identifier)) {
                    $bindings[$identifier_part] = $id;
                } else {
                    throw new \LogicException('Unable to service an entity without its complete identifier.');
                }
            }
        }

        $query  = $builder->build();
        $result = $this->datasource->one($query, $bindings);

        if (null !== $result) {
            $result = $this->create($result);
        }

        return $result;
    }

    /**
     * Create an instance of entity.
     *
     * @param array $data
     *
     * @return Entity
     * @throws QueryBuilderException
     * @throws \LogicException
     */
    protected function create(array $data = []) : Entity
    {
        $entity = new $this->entity_class();
        foreach ($data as $column => $value) {
            $reflection = new \ReflectionMethod($entity, $this->computeSetter($column));
            $class      = $reflection->getParameters()[0]->getClass();
            if ($class && is_subclass_of($class->getName(), 'BullitEngine\Component\Orm\Entity')) {
                $mapper_class = $class->getName().'Mapper';
                /** @var Mapper $mapper */
                $mapper = new $mapper_class($this->datasource);
                $value  = $mapper->get($value);
            }

            if (null !== $value) {
                call_user_func([ $entity, $this->computeSetter($column) ], $value);
            }
        }

        return $entity;
    }

    /**
     * Create the entity in the datasource.
     *
     * @param Entity $entity
     *
     * @return bool
     * @throws QueryBuilderException
     */
    public function insert(Entity $entity) : bool
    {
        $bindings = [];
        foreach ($entity->columns() as $column) {
            $value = call_user_func([ $entity, $column ]);
            if ($value instanceof Entity) {
                /** @noinspection PhpUndefinedMethodInspection */
                $value = $value->id();
            }
            $bindings[$column] = $value;
        }

        $builder = QueryBuilder::factory();
        $builder->insert()
                ->table($this->table)
                ->columns(array_keys($bindings));
        $query = $builder->build();

        $result = $this->datasource->insert($query, $bindings);
        if (true === $result && 1 === count($this->identifier) && null === call_user_func([ $entity, $this->identifier[0] ])) {
            call_user_func([
                $entity,
                $this->computeSetter($this->identifier[0])
            ], $this->datasource->lastInsertedId());
        }

        return $result;
    }

    /**
     * Update the entity in the datasource.
     *
     * @param Entity $entity
     *
     * @return bool
     * @throws QueryBuilderException
     */
    public function update(Entity $entity) : bool
    {
        $bindings = [];
        $columns  = [];
        foreach ($entity->columns() as $column) {
            $value = call_user_func([ $entity, $column ]);
            if ($value instanceof Entity) {
                /** @noinspection PhpUndefinedMethodInspection */
                $value = $value->id();
            }
            $bindings[$column] = $value;

            if (!in_array($column, $this->identifier)) {
                $columns[] = $column;
            }
        }

        $builder = QueryBuilder::factory();
        $builder->update()
                ->table($this->table)
                ->columns($columns)
                ->where();

        foreach ($this->identifier as $identifier_part) {
            $builder->andWhere($builder->formatColumn($identifier_part).' = '.$builder->formatBind($identifier_part));
        }

        $query = $builder->build();

        return $this->datasource->update($query, $bindings);
    }

    /**
     * Delete the entity in the datasource.
     *
     * @param Entity $entity
     *
     * @return bool
     * @throws QueryBuilderException
     */
    public function delete(Entity $entity) : bool
    {
        $builder = QueryBuilder::factory();
        $builder->delete()
                ->from($this->table)
                ->where();

        $bindings = [];
        foreach ($this->identifier as $identifier_part) {
            $bindings[$identifier_part] = call_user_func([
                $entity,
                $identifier_part
            ]);
            $builder->andWhere($builder->formatColumn($identifier_part).' = '.$builder->formatBind($identifier_part));
        }

        $query = $builder->build();

        return $this->datasource->delete($query, $bindings);
    }

    /**
     * @return string
     */
    public function table() : string
    {
        return $this->table;
    }

    /**
     * @return array
     */
    public function identifier() : array
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function entityClass() : string
    {
        return $this->entity_class;
    }
}

