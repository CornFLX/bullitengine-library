<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Config;

use BullitEngine\Component\Config\Exception\FileNotReadableException;


/**
 * Class ConfigParserJson
 *
 * @package BullitEngine\Component\Config
 */
class ConfigParserJson implements ConfigParserInterface
{
    /** @var string */
    private $file;

    /**
     * @param string $file
     *
     * @throws \RuntimeException
     */
    public function __construct(string $file)
    {
        if (!is_readable($file)) {
            throw new FileNotReadableException('Configuration file "'.$file.'" is not readable.');
        }

        $this->file = $file;
    }

    /**
     * Returns an array of parameters.
     * Example : array(
     *   'database.host' => 'localhost',
     *   'database.user' => 'john',
     *   'logger.level'  => 'debug'
     * )
     * @return array
     * @throws \RuntimeException
     */
    public function getConfig() : array
    {
        return $this->flatten($this->decodeJson());
    }

    /**
     * Transform associative array in "flat" array with dot notation.
     *
     * @param array  $arr
     * @param string $prefix
     *
     * @return array
     */
    private function flatten(array $arr, string $prefix = '') : array
    {
        $flat_arr = [];

        foreach ($arr as $key => $value) {
            if ($prefix) {
                $key = $prefix.'.'.$key;
            }

            if (is_array($value)) {
                $flat_arr = $flat_arr + $this->flatten($value, $key);
            } else {
                $flat_arr[$key] = $value;
            }
        }

        return $flat_arr;
    }

    /**
     * Decodes configuration file.
     *
     * @return array
     * @throws \RuntimeException
     */
    private function decodeJson() : array
    {
        $json = json_decode(file_get_contents($this->file), true);
        if (null === $json) {
            throw new \RuntimeException('Unable to parse JSON from "'.$this->file.'".');
        }

        return $json;
    }

    /**
     * @return string
     */
    public function file() : string
    {
        return $this->file;
    }
}

