<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Config\Exception;

/**
 * Class FileNotReadableException
 *
 * @package BullitEngine\Component\Config\Exception
 */
class FileNotReadableException extends \RuntimeException
{
}

