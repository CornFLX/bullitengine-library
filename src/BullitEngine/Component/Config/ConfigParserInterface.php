<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Config;

/**
 * Interface ConfigParserInterface
 *
 * @package BullitEngine\Component\Config
 */
interface ConfigParserInterface
{
    /**
     * Returns an array of parameters.
     * Example : array(
     *   'database.host' => 'localhost',
     *   'database.user' => 'john',
     *   'logger.level'  => 'debug'
     * )
     *
     * @return array
     */
    public function getConfig() : array;
}

