<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Config\Tests;

use BullitEngine\Component\Config\ConfigParserJson;
use BullitEngine\Component\Config\Exception\FileNotReadableException;
use PHPUnit\Framework\TestCase;

class ConfigParserJsonTest extends TestCase
{
    public static function setUpBeforeClass()
    {
        $json = '
        {
            "database.host": "localhost",
            "database.user": "foo",
            "database.passwd": 123456,
            "logging": {
                "level": "debug"
            }
        }';
        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'json_test.json', $json);

        $bad_json = '
        {
            "database.host": "localhost"
            "database.user": "foo"
            "database.passwd": 123456
        }';
        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'json_test_bad.json', $bad_json);
    }

    public static function tearDownAfterClass()
    {
        unlink(__DIR__.DIRECTORY_SEPARATOR.'json_test.json');
        unlink(__DIR__.DIRECTORY_SEPARATOR.'json_test_bad.json');
    }

    public function testGetConfig()
    {
        $parser = new ConfigParserJson(__DIR__.DIRECTORY_SEPARATOR.'json_test.json');

        $expected = [
            'database.host'   => 'localhost',
            'database.user'   => 'foo',
            'database.passwd' => 123456,
            'logging.level'   => 'debug'
        ];
        $this->assertEquals($expected, $parser->getConfig());
    }

    public function testFileNotReadable()
    {
        $this->expectException(FileNotReadableException::class);
        new ConfigParserJson('foo.json');
    }

    public function testBadJsonFormat()
    {
        $this->expectException(\RuntimeException::class);
        $parser = new ConfigParserJson(__DIR__.DIRECTORY_SEPARATOR.'json_test_bad.json');
        $parser->getConfig();
    }

    public function testFile()
    {
        $parser = new ConfigParserJson(__DIR__.DIRECTORY_SEPARATOR.'json_test.json');
        $this->assertEquals(__DIR__.DIRECTORY_SEPARATOR.'json_test.json', $parser->file());
    }
}
