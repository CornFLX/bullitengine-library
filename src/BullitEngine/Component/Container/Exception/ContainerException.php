<?php namespace BullitEngine\Component\Container\Exception;

/**
 * Class ContainerException
 *
 * @package BullitEngine\Component\Container\Exception
 */
class ContainerException extends \RuntimeException
{
}

