<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Container;

use BullitEngine\Component\Container\Exception\ContainerException;

/**
 * Class Container
 *
 * @package BullitEngine\Component\Container
 */
class Container
{
    /** @var Service[] */
    protected $services = [];

    /** @var array */
    protected $instances = [];

    /** @var array */
    protected $parameters = [];

    /** @var array */
    protected $parameters_parsed = null;

    /** @var array */
    protected $aliases = [];

    /**
     * Constructor.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters = [])
    {
        $this->parameters = $parameters;
    }

    /**
     * Register a service.
     *
     * @param Service $service The service to register.
     *
     * @throws ContainerException
     */
    public function register(Service $service) : void
    {
        if ($this->exists($service->name())) {
            throw new ContainerException('Service named "'.$service->name().'" already registered.');
        }

        $this->services[$service->name()] = $service;
    }

    /**
     * Check if a service exists.
     *
     * @param string $name Name of the service.
     *
     * @return bool
     */
    public function exists(string $name) : bool
    {
        return isset($this->services[$name]);
    }

    /**
     * @param string $name
     *
     * @return mixed
     * @throws ContainerException
     */
    public function service(string $name)
    {
        if (!$this->exists($name)) {
            throw new ContainerException('Service "'.$name.'" not registered.');
        }

        $service = $this->services[$name];

        if (!isset($this->instances[$name]) || !$service->isShared()) {
            $opts       = [];
            $parameters = $this->unflattenParameters();
            if (isset($parameters[$name])) {
                $opts = $parameters[$name];
            }

            $this->instances[$name] = $this->callServiceAction($service, $opts);
        }

        return $this->instances[$name];
    }

    /**
     * Transforms $parameters from a flat dotted format to nested array.
     * Example :
     *    array(
     *         database.user => 'foo',
     *         database.host => 'localhost'
     *    )
     *    become:
     *    array(
     *        'database' => array(
     *            'user' => 'foo',
     *            'host' => 'localhost'
     *        )
     *    )
     *
     * @return array
     */
    private function unflattenParameters() : array
    {
        if (null === $this->parameters_parsed) {
            $arr = [];

            foreach ($this->parameters as $name => $value) {
                $indexes = explode('.', $name);
                $sub_arr = [ array_pop($indexes) => $value ];

                foreach (array_reverse($indexes) as $index) {
                    $sub_arr = [ $index => $sub_arr ];
                }

                $arr = array_merge_recursive($arr, $sub_arr);
            }
            $this->parameters_parsed = $arr;
        }

        return $this->parameters_parsed;
    }

    /**
     * Call service's action.
     *
     * @param Service $service
     * @param array   $opts
     *
     * @return mixed
     * @throws ContainerException
     */
    private function callServiceAction(Service $service, array $opts = [])
    {
        $ordered_opts = [];

        $reflect = new \ReflectionFunction($service->action());
        foreach ($reflect->getParameters() as $index => $param) {
            $name = $param->getName();
            if (isset($opts[$name])) {
                $ordered_opts[] = $opts[$name];
            } else if ($param->isDefaultValueAvailable()) {
                $ordered_opts[] = $param->getDefaultValue();
            } else {
                throw new ContainerException('Missing parameter "'.$name.'" for service "'.$service->name().'".');
            }
        }

        return call_user_func_array($service->action(), $ordered_opts);
    }

    /**
     * @param string $service
     * @param string $class
     */
    public function addAlias(string $service, string $class) : void
    {
        $this->aliases[$class] = $service;
    }

    /**
     * @param string $alias
     *
     * @return string
     * @throws \RuntimeException
     */
    public function alias(string $alias) : string
    {
        if (!$this->isAlias($alias)) {
            throw new \RuntimeException('"'.$alias.'" is not an alias.');
        }

        return $this->aliases[$alias];
    }

    /**
     * @param string $alias
     *
     * @return bool
     */
    public function isAlias(string $alias) : bool
    {
        return isset($this->aliases[$alias]);
    }

    /**
     * Get a parameter from its name.
     *
     * @param string $name
     *
     * @return string
     * @throws ContainerException
     */
    public function getParameter(string $name) : string
    {
        if (!isset($this->parameters[$name])) {
            throw new ContainerException('Parameter "'.$name.'" does not exist.');
        }

        return $this->parameters[$name];
    }

    /**
     * @return array
     */
    public function instances() : array
    {
        return $this->instances;
    }
}

