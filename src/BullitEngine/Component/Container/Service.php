<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Container;

/**
 * Class Service
 *
 * @package BullitEngine\Component\Container
 */
class Service
{
    /** @var  callable */
    protected $action;

    /** @var string */
    protected $name;

    /** @var bool */
    protected $is_shared = true;

    /**
     * Constructor.
     *
     * @param string   $name
     * @param callable $action
     */
    public function __construct(string $name, callable $action)
    {
        $this->name   = $name;
        $this->action = $action;
    }

    /**
     * Run a service.
     *
     * @return mixed
     */
    public function run()
    {
        return call_user_func($this->action);
    }

    /**
     * @return callable
     */
    public function action() : callable
    {
        return $this->action;
    }

    /**
     * @return string
     */
    public function name() : string
    {
        return $this->name;
    }

    /**
     * @return boolean
     */
    public function isShared() : bool
    {
        return $this->is_shared;
    }

    /**
     * @param bool $is_shared
     */
    public function setIsShared(bool $is_shared) : void
    {
        $this->is_shared = ($is_shared == true);
    }
}

