<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Container\Tests;

use BullitEngine\Component\Container\Exception\ContainerException;
use BullitEngine\Component\Container\Service;
use BullitEngine\Component\Container\Container;
use PHPUnit\Framework\TestCase;

class ContainerTest extends TestCase
{
    public function testConstructor()
    {
        $container = new Container();
        $this->assertInstanceOf('BullitEngine\\Component\\Container\\Container', $container);
    }

    public function testRegister()
    {
        $service = new Service('foo', function() {
            return new Foo('bar');
        });

        $container = new Container();
        $container->register($service);

        $this->assertTrue($container->exists('foo'));
    }

    public function testRegisterTwice()
    {
        $this->expectException(ContainerException::class);

        $service = new Service('foo', function() {
            return new Foo('bar');
        });

        $container = new Container();
        $container->register($service);
        $container->register($service);
    }

    public function testGet()
    {
        $service   = new Service('foo', function($bar, $optional = true) {
            return new Foo($bar, $optional);
        });
        $container = new Container([ 'foo.bar' => 'bar' ]);
        $container->register($service);

        $this->assertInstanceOf(__NAMESPACE__.'\\Foo', $container->service('foo'));
    }

    public function testGetServiceNotRegistered()
    {
        $this->expectException(ContainerException::class);

        $container = new Container();
        $container->service('foo');
    }

    public function testGetMissingParameter()
    {
        $this->expectException(ContainerException::class);

        $service   = new Service('foo', function($bar) {
            return new Foo($bar);
        });
        $container = new Container();
        $container->register($service);
        $container->service('foo');
    }

    public function testAlias()
    {
        $container = new Container();
        $container->addAlias('foo', 'FooClass');

        $this->assertEquals('foo', $container->alias('FooClass'));
    }

    public function testAliasNotExists()
    {
        $this->expectException(\RuntimeException::class);
        $container = new Container();
        $container->alias('foo');
    }

    public function testInstances()
    {
        $service   = new Service('foo', function($bar) {
            return new Foo($bar);
        });
        $container = new Container([ 'foo.bar' => 'bar' ]);
        $container->register($service);

        $expected = [ 'foo' => $container->service('foo') ];

        $this->assertEquals($expected, $container->instances());
    }
}

class Foo
{
    public function __construct($param, $optional = true)
    {
    }
}
