<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Container\Tests;

use BullitEngine\Component\Container\Service;
use PHPUnit\Framework\TestCase;

class ServiceTest extends TestCase
{
    public function testConstructor()
    {
        $service = new Service('foo', function() {
            return 'foo';
        });
        $this->assertEquals('foo', $service->name());
    }

    public function testRun()
    {
        $service = new Service('foo', function() {
            return 'foo';
        });
        $this->assertEquals('foo', $service->run());
    }
}
