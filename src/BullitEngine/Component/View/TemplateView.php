<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\View;

/**
 * Class TemplateView
 *
 * @package BullitEngine\Component\View
 */
class TemplateView implements TemplateInterface
{
    /** @var array */
    protected $vars;

    /** @var CompilerInterface */
    protected $compiler;

    /** @var string */
    protected $path;

    /** @var string */
    protected $extension = 'bet.html';

    /**
     * Constructor.
     *
     * @param CompilerInterface $compiler
     * @param string            $path
     * @param array             $vars
     *
     * @throws \RuntimeException
     */
    public function __construct(CompilerInterface $compiler, string $path = null, array $vars = [])
    {
        $this->compiler = $compiler;
        $this->vars     = $vars;

        if (null !== $path) {
            $this->setPath($path);
        }
    }

    /**
     * @param string $path
     *
     * @throws \RuntimeException
     */
    public function setPath(string $path) : void
    {
        if (strripos($path, $this->extension) !== (strlen($path) - strlen($this->extension))) {
            throw new \RuntimeException('Template must have "'.$this->extension.'" extension.');
        }

        $this->path = realpath($path);
    }

    /**
     * @param array $vars
     */
    public function addVars(array $vars) : void
    {
        $this->vars = array_merge($this->vars, $vars);
    }

    /**
     * @param $name
     * @param $value
     */
    public function addVar(string $name, $value) : void
    {
        $this->vars[$name] = $value;
    }

    /**
     * Render a view.
     *
     * @return string
     */
    public function render() : string
    {
        $compiled_path = $this->compiler->compiledPath($this->path);
        if (!is_readable($compiled_path)) {
            $this->compiler->compile($this->path);
        }

        extract($this->vars);
        ob_start();
        /** @noinspection PhpIncludeInspection */
        include $compiled_path;

        return ob_get_clean();
    }

    /**
     * @return string
     */
    public function extension() : string
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension(string $extension) : void
    {
        $this->extension = $extension;
    }
}

