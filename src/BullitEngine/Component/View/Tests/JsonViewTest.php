<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\View\Tests;

use BullitEngine\Component\View\JsonView;
use PHPUnit\Framework\TestCase;

class JsonViewTest extends TestCase
{
    public function testSetGet()
    {
        $view        = new JsonView();
        $view['foo'] = 'bar';
        $view[]      = 'bar2';

        $this->assertTrue(isset($view['foo']));
        $this->assertEquals('bar', $view['foo']);
        $this->assertEquals('bar2', $view[0]);
    }

    public function testUnset()
    {
        $view        = new JsonView();
        $view['foo'] = 'bar';
        unset($view['foo']);

        $this->assertArrayNotHasKey('foo', $view);
    }

    public function testSet()
    {
        $view = new JsonView();

        $values = [
            'foo' => 'bar'
        ];

        $view->set($values);

        $this->assertTrue(isset($view['foo']));
        $this->assertEquals('bar', $view['foo']);
    }

    public function testRender()
    {
        $view         = new JsonView();
        $view['foo']  = 'bar';
        $view['foo2'] = 'bar2';

        $expected = '{"foo":"bar","foo2":"bar2"}';
        $this->assertEquals($expected, $view->render());
    }
}
