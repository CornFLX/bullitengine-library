<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\View\Tests;

use BullitEngine\Component\View\Compiler;
use PHPUnit\Framework\TestCase;

class CompilerTest extends TestCase
{
    public function testCompiledPath()
    {
        $compiler = new Compiler(__DIR__);
        $this->assertEquals(__DIR__.DIRECTORY_SEPARATOR.'5eaf33ebae1e839d9ae757b45b1931d7', $compiler->compiledPath('views'.DIRECTORY_SEPARATOR.'foo.bet.html'));
    }

    public function testCompileNonExistantPath()
    {
        $path = __DIR__.DIRECTORY_SEPARATOR.'cache';
        new Compiler($path);
        $this->assertDirectoryExists($path);
        $this->assertDirectoryIsReadable($path);
        $this->assertDirectoryIsWritable($path);

        rmdir($path);
    }

    public function testCompile()
    {
        $tpl      = <<<'HTML'
{% if (is_array($var)) %}
    $var is an array.
    {% foreach ($var as $item) %}
        {{ $item }}
    {% endforeach %}
{% elseif (is_object($var)) %}
    $var is an object.
{% else %}
    $var is equal to "{{ $var }}".
{% endif %}
{% for($i = 0; $i < 10; $i++) %}
    {{ $i }}
{% endfor %}
<?php echo 'bar'; ?>
HTML;
        $expected = <<<'HTML'
<?php if(is_array($var)): ?>
    $var is an array.
    <?php foreach($var as $item): ?>
        <?php print $item; ?>
    <?php endforeach; ?>
<?php elseif(is_object($var)): ?>
    $var is an object.
<?php else: ?>
    $var is equal to "<?php print $var; ?>".
<?php endif; ?>
<?php for($i = 0; $i < 10; $i++): ?>
    <?php print $i; ?>
<?php endfor; ?>
<?php echo 'bar'; ?>
HTML;

        file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'foo.bet.html', $tpl);

        $compiler = new Compiler(__DIR__);
        $compiler->compile(__DIR__.DIRECTORY_SEPARATOR.'foo.bet.html');

        $actual = file_get_contents($compiler->compiledPath(__DIR__.DIRECTORY_SEPARATOR.'foo.bet.html'));
        unlink(__DIR__.DIRECTORY_SEPARATOR.'foo.bet.html');
        unlink($compiler->compiledPath(__DIR__.DIRECTORY_SEPARATOR.'foo.bet.html'));

        $this->assertEquals($expected, $actual);
    }
}
