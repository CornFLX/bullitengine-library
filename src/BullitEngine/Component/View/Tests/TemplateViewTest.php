<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\View\Tests;

use BullitEngine\Component\View\Compiler;
use BullitEngine\Component\View\TemplateView;
use PHPUnit\Framework\TestCase;

class TemplateViewTest extends TestCase
{
    public function testAddVar()
    {
        $compiler = new Compiler(__DIR__);

        $view = new TemplateView($compiler, 'foo.bet.html');
        $view->addVar('foo', 'bar');

        $this->assertAttributeEquals([ 'foo' => 'bar' ], 'vars', $view);
    }

    public function testAddVars()
    {
        $compiler = new Compiler(__DIR__);

        $view = new TemplateView($compiler, 'foo.bet.html');
        $view->addVars([ 'foo' => 'bar' ]);

        $this->assertAttributeEquals([ 'foo' => 'bar' ], 'vars', $view);
    }

    public function testRender()
    {
        $path = __DIR__.DIRECTORY_SEPARATOR.'foo.bet.html';
        file_put_contents($path, 'hello');

        $compiler = new Compiler(__DIR__);
        $view     = new TemplateView($compiler, $path);
        $this->assertEquals('hello', $view->render());

        unlink($path);
        unlink($compiler->compiledPath($path));
    }

    public function testBadExtension()
    {
        $this->expectException(\RuntimeException::class);

        $path = __DIR__.DIRECTORY_SEPARATOR.'foo.html';

        $compiler = new Compiler(__DIR__);
        new TemplateView($compiler, $path);
    }

    public function testExtension()
    {
        $compiler = new Compiler(__DIR__);
        $template = new TemplateView($compiler);

        $template->setExtension('foo.bar.html');

        $this->assertEquals('foo.bar.html', $template->extension());
    }
}
