<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\View\Tests;

use BullitEngine\Component\View\TextView;
use PHPUnit\Framework\TestCase;

class TextViewTest extends TestCase
{

    public function testRender()
    {
        $view = new TextView();
        $view->add('Hello ');
        $view->add('world');

        $this->assertEquals('Hello world', $view->render());
    }
}
