<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\View\Exception;

/**
 * Class ResourceNotFoundException
 *
 * @package BullitEngine\Component\View\Exception
 */
class ResourceNotFoundException extends \RuntimeException
{
}

