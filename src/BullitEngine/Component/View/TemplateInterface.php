<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\View;

/**
 * Interface TemplateInterface
 *
 * @package BullitEngine\Component\View
 */
interface TemplateInterface extends ViewInterface
{
    /**
     * @param array $vars
     */
    public function addVars(array $vars) : void;

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function addVar(string $name, $value) : void;
}

