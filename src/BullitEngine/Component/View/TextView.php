<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\View;

/**
 * Class TextView
 *
 * @package BullitEngine\Component\View
 */
class TextView implements ViewInterface
{
    /** @var string */
    private $content = '';

    /**
     * Add a text in the view.
     *
     * @param string $s
     */
    public function add(string $s) : void
    {
        $this->content .= $s;
    }

    /**
     * Render a view.
     *
     * @return string
     */
    public function render() : string
    {
        return $this->content;
    }
}

