<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\View;

/**
 * Class Compiler
 *
 * @package BullitEngine\Component\View\Compiler
 */
class Compiler implements CompilerInterface
{
    /** @var string */
    protected $cache_path;

    /**
     * The constructor.
     *
     * @param string $cache_path
     */
    public function __construct(string $cache_path)
    {
        $this->setCachePath($cache_path);
    }

    /**
     * Compile a file.
     *
     * @param string $path The filepath to compile.
     */
    public function compile(string $path) : void
    {
        $path    = realpath($path);
        $content = file_get_contents($path);

        $compiled = '';
        foreach (token_get_all($content) as $token) {
            if (is_array($token) && $token[0] === T_INLINE_HTML) {
                $compiled = $this->compileStatements($token[1]);
                $compiled = $this->compileEchos($compiled);
            } else {
                $compiled .= is_array($token) ? $token[1] : $token;
            }
        }

        file_put_contents($this->cache_path.DIRECTORY_SEPARATOR.md5($path), $compiled);
    }

    /**
     * @param $value
     *
     * @return string
     */
    private function compileStatements(string $value) : string
    {
        $callback = function($matches) {
            $identifier = $matches['id'];
            $expression = isset($matches['expr']) ? $matches['expr'] : '';

            $replacement = $matches[0];

            if (method_exists($this, $method = 'compile'.ucfirst(strtolower($identifier)))) {
                $replacement = $this->$method($expression);
            }

            return $replacement;
        };

        return preg_replace_callback('#\B{%\s(?<id>\w+)([ \t]*)(?<expr>\(((?>[^()]+)|(?3))*\))?\s%}#', $callback, $value);
    }

    /**
     * @param string $path
     */
    private function setCachePath(string $path) : void
    {
        if (!is_dir($path)) {
            mkdir($path, 0774, true);
        }
        $this->cache_path = $path;
    }

    /**
     * @param string $value
     *
     * @return string
     */
    private function compileEchos(string $value) : string
    {
        $callback = function($matches) {
            return '<?php print '.$matches['expr'].'; ?>';
        };

        return preg_replace_callback('#{{ (?<expr>.+?) }}#', $callback, $value);
    }

    /**
     * @param $path
     *
     * @return mixed
     */
    public function compiledPath(string $path = null) : string
    {
        $compiled_path = $this->cache_path;
        if (null !== $path) {
            $compiled_path .= DIRECTORY_SEPARATOR.md5($path);
        }

        return $compiled_path;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @param string $expression
     *
     * @return string
     */
    private function compileForeach(string $expression) : string
    {
        return '<?php foreach'.$expression.': ?>';
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @return string
     */
    private function compileEndforeach() : string
    {
        return '<?php endforeach; ?>';
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @param string $expression
     *
     * @return string
     */
    private function compileFor(string $expression) : string
    {
        return '<?php for'.$expression.': ?>';
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @return string
     */
    private function compileEndfor() : string
    {
        return '<?php endfor; ?>';
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @param string $expression
     *
     * @return string
     */
    private function compileIf(string $expression) : string
    {
        return '<?php if'.$expression.': ?>';
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @param string $expression
     *
     * @return string
     */
    private function compileElseIf(string $expression) : string
    {
        return '<?php elseif'.$expression.': ?>';
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @return string
     */
    private function compileElse() : string
    {
        return '<?php else: ?>';
    }

    /** @noinspection PhpUnusedPrivateMethodInspection
     * @return string
     */
    private function compileEndif() : string
    {
        return '<?php endif; ?>';
    }
}

