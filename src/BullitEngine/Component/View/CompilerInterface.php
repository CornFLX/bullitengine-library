<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\View;

/**
 * Interface CompilerInterface
 *
 * @package BullitEngine\Component\View\Compiler
 */
interface CompilerInterface
{
    /**
     * @param string $path The filepath to compile.
     */
    public function compile(string $path) : void;

    /**
     * @param $path
     *
     * @return string
     */
    public function compiledPath(string $path = null) : string;
}

