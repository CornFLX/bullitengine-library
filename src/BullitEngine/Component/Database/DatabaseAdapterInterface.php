<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Database;

/**
 * Interface DatabaseAdapterInterface
 */
interface DatabaseAdapterInterface
{
    /**
     * Executes an SQL statement.
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return bool True on success or false on failure.
     */
    public function statement(string $query, array $bindings = []) : bool;

    /**
     * Executes an SQL statement and returns number of affected rows.
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return int Number of affected rows.
     */
    public function affectedStatement(string $query, array $bindings = []) : int;

    /**
     * Executes a select statement and return a single result.
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return mixed The return value depends on the fetch type.
     */
    public function one(string $query, array $bindings = []);

    /**
     * Executes a select statement
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return array
     */
    public function select(string $query, array $bindings = []) : array;

    /**
     * Executes an insert statement.
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return bool True on success or false on failure.
     */
    public function insert(string $query, array $bindings = []) : bool;

    /**
     * Executes an update statement.
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return int Number of affected rows.
     */
    public function update(string $query, array $bindings = []) : int;

    /**
     * Executes a delete statement.
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return int Number of affected rows.
     */
    public function delete(string $query, array $bindings = []) : int;

    /**
     * Return identifier from the last insert.
     *
     * @return mixed
     */
    public function lastInsertedId();
}

