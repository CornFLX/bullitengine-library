<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Database;

use BullitEngine\Component\Database\Exception\QueryBuilderException;

/**
 * Class QueryBuilder
 *
 * @package BullitEngine\Component\Database
 */
abstract class QueryBuilder
{
    /**
     * Type of the query (SELECT, INSERT OR UPDATE)
     *
     * @var string
     */
    protected $type;

    /** @var string */
    protected $table;

    /** @var array */
    protected $columns = [];

    /** @var array */
    protected $where;

    /**
     * Contains the current query's context.
     *
     * @var string
     */
    protected $current;

    /**
     * @return QueryBuilder
     */
    static public function factory() : QueryBuilder
    {
        return new QueryBuilderMysql();
    }

    /**
     * Start to build a select query.
     *
     * @param array $fields
     *
     * @return QueryBuilder
     * @throws QueryBuilderException
     */
    public function select(array $fields = []) : QueryBuilder
    {
        $this->type    = 'SELECT';
        $this->current = 'select';

        $this->columns($fields);

        return $this;
    }

    /**
     * Define the _columns used.
     *
     * @param array $columns
     *
     * @return QueryBuilder
     * @throws QueryBuilderException
     */
    public function columns(array $columns = []) : QueryBuilder
    {
        $this->checkContext([ 'update', 'insert', 'select' ]);

        $this->columns = $columns;

        return $this;
    }

    /**
     * Check if a method is usable.
     *
     * @param array $contexts_list
     *
     * @throws QueryBuilderException
     */
    private function checkContext(array $contexts_list) : void
    {
        if (!in_array($this->current, $contexts_list)) {
            $backtrace = debug_backtrace();
            throw new QueryBuilderException(__CLASS__.'::'.$backtrace[1]['function'].'() is not usable in this context.');
        }
    }

    /**
     * Start to build an insert query.
     *
     * @return QueryBuilder
     */
    public function insert() : QueryBuilder
    {
        $this->type    = 'INSERT';
        $this->current = 'insert';

        return $this;
    }

    /**
     * Start to build an update query.
     *
     * @return QueryBuilder
     */
    public function update() : QueryBuilder
    {
        $this->type    = 'UPDATE';
        $this->current = 'update';

        return $this;
    }

    /**
     * Start to build a delete query.
     *
     * @return QueryBuilder
     */
    public function delete() : QueryBuilder
    {
        $this->type    = 'DELETE';
        $this->current = 'delete';

        return $this;
    }

    /**
     * Alias of QueryBuilder::table
     *
     * @param $table
     *
     * @return QueryBuilder
     */
    public function from(string $table) : QueryBuilder
    {
        return $this->table($table);
    }

    /**
     * Specify the table of the query.
     *
     * @param $table
     *
     * @return QueryBuilder
     */
    public function table(string $table) : QueryBuilder
    {
        $this->table = $table;

        return $this;
    }

    /**
     * Start a where clause.
     *
     * @param string $condition
     *
     * @return QueryBuilder
     * @throws QueryBuilderException
     */
    public function where(string $condition = null) : QueryBuilder
    {
        if (null !== $this->where) {
            throw new QueryBuilderException('Where clause already initialized.');
        }

        $this->checkContext([ 'update', 'delete', 'select' ]);

        $this->current = 'where';

        if (null === $condition) {
            $this->where = [];
        } else {
            $this->where = [ $condition ];
        }

        return $this;
    }

    /**
     * Add an "and" condition to the where clause.
     *
     * @param string $condition
     *
     * @return QueryBuilder
     * @throws QueryBuilderException
     */
    public function andWhere(string $condition) : QueryBuilder
    {
        $this->checkContext([ 'where' ]);

        if (count($this->where) > 0) {
            $this->where[] = 'AND';
        }

        $this->where[] = $condition;

        return $this;
    }

    /**
     * Add an "or" condition to the where clause.
     *
     * @param string $condition
     *
     * @return QueryBuilder
     * @throws QueryBuilderException
     */
    public function orWhere(string $condition) : QueryBuilder
    {
        $this->checkContext([ 'where' ]);

        if (count($this->where) > 0) {
            $this->where[] = 'OR';
        }
        $this->where[] = $condition;

        return $this;
    }

    /**
     * Build query.
     *
     * @return string
     */
    abstract public function build() : string;

    /**
     * Format a table name.
     *
     * @param string $table
     *
     * @return string
     */
    abstract public function formatTable(string $table) : string;

    /**
     * Format a column name.
     *
     * @param string $column
     *
     * @return string
     */
    abstract public function formatColumn(string $column) : string;

    /**
     * Format a bind.
     *
     * @param string $bind
     *
     * @return string
     */
    abstract public function formatBind(string $bind) : string;
}

