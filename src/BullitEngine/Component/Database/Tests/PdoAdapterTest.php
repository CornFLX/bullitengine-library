<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Database\Tests;

use BullitEngine\Component\Database\PdoAdapter;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

class PdoAdapterTest extends TestCase
{
    use TestCaseTrait;

    public function testConstructor()
    {
        $db = new PdoAdapter($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
        $this->assertInstanceOf('BullitEngine\Component\Database\DatabaseAdapterInterface', $db);
    }

    public function testSelect()
    {
        $db      = new PdoAdapter($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
        $current = $db->select('SELECT id, str FROM table_test');
        $current = $this->createArrayDataSet([ 'table_test' => $current ]);

        $expected = $this->getDataSet();
        $this->assertTablesEqual($expected->getTable('table_test'), $current->getTable('table_test'));
    }

    protected function getDataSet()
    {
        return $this->createArrayDataSet([
            'table_test' => [
                [ 'id' => 1, 'str' => 'aaa' ],
                [ 'id' => 2, 'str' => 'bbb' ],
                [ 'id' => 3, 'str' => 'ccc' ]
            ]
        ]);
    }

    public function testOne()
    {
        $db      = new PdoAdapter($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
        $result  = $db->one('SELECT id, str FROM table_test');
        $current = $this->createArrayDataSet([ 'table_test' => [ $result ] ])
                        ->getTable('table_test');

        $expected = $this->getConnection()
                         ->createQueryTable('table_test', 'SELECT id, str FROM table_test LIMIT 1');
        $this->assertTablesEqual($expected, $current);
    }

    protected function getConnection()
    {
        $db = new \PDO($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);

        return $this->createDefaultDBConnection($db);

    }

    public function testInsert()
    {
        $this->assertEquals(3, $this->getConnection()
                                    ->getRowCount('table_test'));

        $db = new PdoAdapter($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
        $db->insert('INSERT INTO table_test (str) VALUES ("ddd")');

        $this->assertEquals(4, $this->getConnection()
                                    ->getRowCount('table_test'));
    }

    public function testUpdate()
    {
        $db = new PdoAdapter($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
        $this->assertEquals('aaa', $db->one('SELECT str FROM table_test WHERE id=1')['str']);

        $affected = $db->update('UPDATE table_test SET str="aaaa" WHERE id=1');

        $this->assertEquals(1, $affected);
        $this->assertEquals('aaaa', $db->one('SELECT str FROM table_test WHERE id=1')['str']);
    }

    public function testLastInsertId()
    {
        $db = new PdoAdapter($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
        $db->insert('INSERT INTO table_test (str) VALUES ("eee")');

        $this->assertEquals(4, $db->lastInsertedId());
    }

    public function testDelete()
    {
        $db = new PdoAdapter($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
        $this->assertEquals(1, $db->delete('DELETE FROM table_test WHERE id = 3'));
    }

    public function testSetFetchStyle()
    {
        $db = new PdoAdapter($GLOBALS['DB_DSN'], $GLOBALS['DB_USER'], $GLOBALS['DB_PASSWD']);
        $db->setFetchStyle(\PDO::FETCH_NUM);

        $result   = $db->one('SELECT id, str FROM table_test');
        $expected = [ '1', 'aaa' ];

        $this->assertEquals($expected, $result);
    }
}
