<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Database\Tests;

use BullitEngine\Component\Database\Exception\QueryBuilderException;
use BullitEngine\Component\Database\QueryBuilder;
use BullitEngine\Component\Database\QueryBuilderMysql;
use PHPUnit\Framework\TestCase;

class QueryBuilderMysqlTest extends TestCase
{
    public function testFactory()
    {
        $this->assertInstanceOf('BullitEngine\Component\Database\QueryBuilderMysql', QueryBuilder::factory());
    }

    public function testInsert()
    {
        $builder = new QueryBuilderMysql();
        $builder->insert()
                ->table('test')
                ->columns([ 'foo', 'bar' ]);
        $this->assertEquals('INSERT INTO `test` (`foo`, `bar`) VALUES (:foo, :bar)', $builder->build());
    }

    public function testInsertMissingTable()
    {
        $this->expectException(QueryBuilderException::class);

        $builder = new QueryBuilderMysql();
        $builder->insert()
                ->columns([ 'foo' ])
                ->build();
    }

    public function testBuildMissingType()
    {
        $this->expectException(QueryBuilderException::class);

        $builder = $this->getMockBuilder('BullitEngine\Component\Database\QueryBuilderMysql')
                        ->setMethods([ 'insert' ])
                        ->getMock();

        $builder->expects($this->any())
                ->method('insert')
                ->willReturnSelf();

        /** @noinspection PhpUndefinedMethodInspection */
        $builder->insert()
                ->table('test')
                ->build();
    }

    public function testUpdate()
    {
        $builder = new QueryBuilderMysql();
        $builder->update()
                ->table('test')
                ->columns([ 'foo', 'bar' ]);
        $this->assertEquals('UPDATE `test` SET `foo` = :foo, `bar` = :bar', $builder->build());
    }

    public function testUpdateMissingTable()
    {
        $this->expectException(QueryBuilderException::class);

        $builder = new QueryBuilderMysql();
        $builder->update()
                ->columns([ 'foo', 'bar' ]);
        $builder->build();
    }

    public function testUpdateWithWhereClause()
    {
        $builder = new QueryBuilderMysql();
        $builder->update()
                ->table('test')
                ->columns([ 'foo', 'bar' ])
                ->where('bar = 5');
        $this->assertEquals('UPDATE `test` SET `foo` = :foo, `bar` = :bar WHERE bar = 5', $builder->build());
    }

    public function testBadContext()
    {
        $this->expectException(QueryBuilderException::class);

        $builder = new QueryBuilderMysql();
        $builder->insert()
                ->where();
        $builder->build();
    }

    public function testWhere()
    {
        $builder = new QueryBuilderMysql();
        $builder->update()
                ->table('test')
                ->columns([ 'foo', 'bar' ])
                ->where()
                ->andWhere('bar = 5')
                ->orWhere('foo = 3')
                ->andWhere('NOW() > 2015-01-01');
        $this->assertEquals('UPDATE `test` SET `foo` = :foo, `bar` = :bar WHERE bar = 5 OR foo = 3 AND NOW() > 2015-01-01', $builder->build());
    }

    public function testWhereAlreadyInitialized()
    {
        $this->expectException(QueryBuilderException::class);

        $builder = new QueryBuilderMysql();
        $builder->update()
                ->table('test')
                ->columns([ 'foo', 'bar' ])
                ->where()
                ->where();
    }

    public function testDelete()
    {
        $builder = new QueryBuilderMysql();
        $builder->delete()
                ->from('test')
                ->where('id = 3');

        $this->assertEquals('DELETE FROM `test` WHERE id = 3', $builder->build());
    }

    public function testSelect()
    {
        $builder = new QueryBuilderMysql();
        $builder->select([ '*' ])
            ->from('test')
            ->where('id = 3');

        $this->assertEquals('SELECT * FROM `test` WHERE id = 3', $builder->build());
    }
}
