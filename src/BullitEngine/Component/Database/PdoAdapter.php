<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Database;

use PDO;

/**
 * Class PdoAdapter
 *
 * @package BullitEngine\Component\Database
 */
class PdoAdapter implements DatabaseAdapterInterface
{
    /** @var PDO */
    private $pdo;

    /** @var int */
    private $fetch_style = PDO::FETCH_ASSOC;

    /**
     * Constructor. Connect to the database.
     *
     * @param string $dsn The Data Source Name
     * @param string $username The user name
     * @param string $password The password
     * @param array  $options Driver-specific options
     */
    public function __construct($dsn, $username, $password, array $options = [])
    {
        $this->pdo = new PDO($dsn, $username, $password, $options);
    }

    /**
     * Executes a select statement and return a single result.
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return mixed The return value depends on the fetch type.
     */
    public function one(string $query, array $bindings = [])
    {
        $all = $this->select($query, $bindings);

        return count($all) > 0 ? array_shift($all) : null;
    }

    /**
     * Executes a select statement
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return array
     */
    public function select(string $query, array $bindings = []) : array
    {
        $statement = $this->pdo->prepare($query);
        $statement->execute($bindings);

        return $statement->fetchAll($this->fetch_style);
    }

    /**
     * Executes an insert statement.
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return bool True on success or false on failure.
     */
    public function insert(string $query, array $bindings = []) : bool
    {
        return $this->statement($query, $bindings);
    }

    /**
     * Executes an SQL statement.
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return bool True on success or false on failure.
     */
    public function statement(string $query, array $bindings = []) : bool
    {
        $statement = $this->pdo->prepare($query);

        return $statement->execute($bindings);
    }

    /**
     * Executes an update statement.
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return int Number of affected rows.
     */
    public function update(string $query, array $bindings = []) : int
    {
        return $this->affectedStatement($query, $bindings);
    }

    /**
     * Executes an SQL statement and returns number of affected rows.
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return int Number of affected rows.
     */
    public function affectedStatement(string $query, array $bindings = []) : int
    {
        $statement = $this->pdo->prepare($query);
        $statement->execute($bindings);

        return $statement->rowCount();
    }

    /**
     * Executes a delete statement.
     *
     * @param string $query SQL statement
     * @param array  $bindings Array of values to bind
     *
     * @return int Number of affected rows.
     */
    public function delete(string $query, array $bindings = []) : int
    {
        return $this->affectedStatement($query, $bindings);
    }

    /**
     * @param int $fetch_style
     */
    public function setFetchStyle($fetch_style) : void
    {
        $this->fetch_style = $fetch_style;
    }

    /**
     * Return identifier from the last insert.
     *
     * @return mixed
     */
    public function lastInsertedId()
    {
        return $this->pdo->lastInsertId();
    }
}
