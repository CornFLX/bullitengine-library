<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Database;

use BullitEngine\Component\Database\Exception\QueryBuilderException;

/**
 * Class QueryBuilderMysql
 *
 * @package BullitEngine\Component\Database
 */
class QueryBuilderMysql extends QueryBuilder
{
    const FORMAT_TABLE  = '`%s`';
    const FORMAT_COLUMN = '`%s`';
    const FORMAT_BIND   = ':%s';

    /**
     * Build query.
     *
     * @return string
     * @throws QueryBuilderException
     */
    public function build() : string
    {
        if (null === $this->type) {
            throw new QueryBuilderException('Missing query\'s type.');
        }

        if (null === $this->table) {
            throw new QueryBuilderException('Missing table name.');
        }

        $build_method = 'build'.ucfirst(strtolower($this->type));

        return $this->$build_method();
    }

    /**
     * Build an insert query.
     *
     * @return string
     */
    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function buildInsert() : string
    {
        $table      = $this->formatTable($this->table);
        $columns    = [];
        $parameters = [];

        foreach ($this->columns as $column) {
            $columns[]    = $this->formatColumn($column);
            $parameters[] = $this->formatBind($column);
        }

        $columns    = implode(', ', $columns);
        $parameters = implode(', ', $parameters);

        return 'INSERT INTO '.$table.' ('.$columns.') VALUES ('.$parameters.')';
    }

    /**
     * Format a table name.
     *
     * @param string $table
     *
     * @return string
     */
    public function formatTable(string $table) : string
    {
        return sprintf(self::FORMAT_TABLE, $table);
    }

    /**
     * Format a column name.
     *
     * @param string $column
     *
     * @return string
     */
    public function formatColumn(string $column) : string
    {
        return sprintf(self::FORMAT_COLUMN, $column);
    }

    /**
     * Format a bind.
     *
     * @param string $bind
     *
     * @return string
     */
    public function formatBind(string $bind) : string
    {
        return sprintf(self::FORMAT_BIND, $bind);
    }

    /**
     * Build an update query.
     *
     * @return string
     */
    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function buildUpdate() : string
    {
        $table      = $this->formatTable($this->table);
        $parameters = '';

        foreach ($this->columns as $column) {
            $parameters .= $this->formatColumn($column).' = '.$this->formatBind($column).', ';
        }
        $parameters = substr($parameters, 0, -2);

        $query = 'UPDATE '.$table.' SET '.$parameters;
        if (count($this->where) > 0) {
            $query .= ' WHERE '.implode(' ', $this->where);
        }

        return $query;
    }

    /**
     * Build a delete query.
     *
     * @return string
     */
    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function buildDelete() : string
    {
        $table = $this->formatTable($this->table);

        $query = 'DELETE FROM '.$table;
        if (count($this->where) > 0) {
            $query .= ' WHERE '.implode(' ', $this->where);
        }

        return $query;
    }

    /**
     * Build a select query.
     *
     * @return string
     */
    /** @noinspection PhpUnusedPrivateMethodInspection */
    private function buildSelect() : string
    {
        $table  = $this->formatTable($this->table);
        $fields = '';

        foreach ($this->columns as $column) {
            $fields .= $column.', ';
        }
        $fields = substr($fields, 0, -2);

        $query = 'SELECT '.$fields.' FROM '.$table;
        if (count($this->where) > 0) {
            $query .= ' WHERE '.implode(' ', $this->where);
        }

        return $query;
    }
}
