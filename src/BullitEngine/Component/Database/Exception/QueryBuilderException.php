<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Database\Exception;

/**
 * Class QueryBuilderException
 *
 * @package BullitEngine\Component\Database\Exception
 */
class QueryBuilderException extends \RuntimeException
{
}

