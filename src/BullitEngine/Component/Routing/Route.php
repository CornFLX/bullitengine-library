<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Routing;

use Closure;

/**
 * Class Route
 *
 * @package BullitEngine\Component\Routing
 */
class Route
{
    /**
     * The path pattern.
     *
     * @var string
     */
    protected $path;

    /**
     * Retricted methods.
     *
     * @var array
     */
    protected $restricted_methods;

    /**
     * Action to execute.
     *
     * @var Closure
     */
    protected $action;

    /**
     * Constructor
     *
     * @param string         $path The path to match
     * @param Closure|string $action
     * @param array          $restricted_methods Array of restricted methods
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(string $path, $action, array $restricted_methods = [])
    {
        $this->setPath($path);
        $this->setAction($action);
        $this->setRestrictedMethods($restricted_methods);
    }

    /**
     * @param array $methods
     */
    public function setRestrictedMethods(array $methods) : void
    {
        $this->restricted_methods = array_map('strtoupper', $methods);
    }

    /**
     * @return string
     */
    public function path() : string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path) : void
    {
        $this->path = trim($path);
    }

    /**
     * @return array
     */
    public function methods() : array
    {
        return $this->restricted_methods;
    }

    /**
     * @return callable
     */
    public function action() : callable
    {
        return $this->action;
    }

    /**
     * @param Closure|string $action
     *
     * @throws \InvalidArgumentException
     */
    public function setAction($action) : void
    {
        if (!is_callable($action) && !is_string($action)) {
            throw new \InvalidArgumentException('Parameter "action" must be a Callable or string.');
        }

        if (!is_callable($action) && false !== strpos($action, '@')) {
            $action_org = $action;
            if (null === $action = $this->findCallable($action)) {
                $action_exploded = explode('@', $action_org);
                throw new \InvalidArgumentException('Method "'.$action_exploded[0].'::'.$action_exploded[1].'" does not exists.');
            }
        } else if (is_string($action) && !function_exists($action)) {
            throw new \InvalidArgumentException('Function "'.$action.'" does not exists.');
        }

        if (is_array($action) && !is_object($action[0])) {
            $action[0] = new $action[0]();
        }

        $this->action = $action;
    }

    /**
     * Find a callable from a "class@method" formatted action.
     *
     * @param Closure|string $action
     *
     * @return array|null
     */
    private function findCallable($action) : ?array
    {
        $action_exploded = explode('@', $action);

        $classnames_to_try = [
            'App\\Controllers\\'.$action_exploded[0],
            $action_exploded[0]
        ];

        foreach ($classnames_to_try as $classname) {
            if (method_exists($classname, $action_exploded[1])) {
                return [ $classname, $action_exploded[1] ];
            }
        }

        return null;
    }
}
