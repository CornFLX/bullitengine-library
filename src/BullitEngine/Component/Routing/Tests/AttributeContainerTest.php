<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Routing\Tests;


use BullitEngine\Component\Routing\AttributeContainer;
use PHPUnit\Framework\TestCase;

class AttributeContainerTest extends TestCase
{
    public function testGet()
    {
        $attributes = new AttributeContainer([ 'foo' => 'bar' ]);
        $this->assertEquals('bar', $attributes->get('foo'));
        $this->assertEquals(null, $attributes->get('foo2'));
    }

    public function testSet()
    {
        $attributes = new AttributeContainer();
        $attributes->set('foo', 'bar');
        $this->assertEquals('bar', $attributes->get('foo'));
    }

    public function testAll()
    {
        $attributes = new AttributeContainer([
            'foo'  => 'bar',
            'foo2' => 'bar2',
            'foo3' => 'bar3'
        ]);
        $this->assertEquals('bar', $attributes->all()['foo']);
        $this->assertEquals('bar2', $attributes->all()['foo2']);
        $this->assertEquals('bar3', $attributes->all()['foo3']);
    }

    public function testGetIterator()
    {
        $attributes = new AttributeContainer([ 'foo' => 'bar' ]);
        foreach ($attributes as $name => $value) {
            $this->assertEquals('foo', $name);
            $this->assertEquals('bar', $value);
        }
    }

    public function testCount()
    {
        $attributes = new AttributeContainer([
            'foo'  => 'bar',
            'foo2' => 'bar2',
            'foo3' => 'bar3'
        ]);
        $this->assertEquals(3, count($attributes));
    }
}
