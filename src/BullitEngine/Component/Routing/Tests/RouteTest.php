<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Routing\Tests;


use BullitEngine\Component\Routing\Route;
use PHPUnit\Framework\TestCase;

class RouteTest extends TestCase
{
    public function testConstructor()
    {
        $foo = function() {
        };

        $route = new Route('/foo', $foo);
        $this->assertEquals('/foo', $route->path());
        $this->assertEquals($foo, $route->action());
        $this->assertEmpty($route->methods());
    }

    public function testConstructorWithRestrictedMethods()
    {
        $foo = function() {
        };

        $route = new Route('/foo', $foo, [ 'GET' ]);
        $this->assertEquals('/foo', $route->path());
        $this->assertEquals($foo, $route->action());
        $this->assertCount(1, $route->methods());
        $this->assertEquals('GET', $route->methods()[0]);
    }

    public function testInvalidAction()
    {
        $this->expectException(\InvalidArgumentException::class);
        new Route('/foo', [ 'not_a', 'callable' ]);
    }

    public function testSetAction()
    {
        $mock = $this->getMockBuilder('DummyClass')
                     ->setMethods([ 'foo' ])
                     ->getMock();

        $actions = [
            'var_dump',
            function() {
            },
            [ $mock, 'foo' ],
            get_class($mock).'@foo'
        ];

        foreach ($actions as $action) {
            new Route('/foo', $action);
        }

        $this->assertTrue(true);
    }

    public function testSetActionWithBadMethod()
    {
        $this->expectException(\InvalidArgumentException::class);

        new Route('/foo', 'Foo@bar');
    }

    public function testSetActionWithBadFunction()
    {
        $this->expectException(\InvalidArgumentException::class);

        new Route('/foo', 'foo');
    }
}
