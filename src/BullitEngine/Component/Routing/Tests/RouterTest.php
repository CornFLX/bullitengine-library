<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Routing\Tests;

use BullitEngine\Component\Container\Container;
use BullitEngine\Component\Container\Exception\ContainerException;
use BullitEngine\Component\Database\DatabaseAdapterInterface;
use BullitEngine\Component\Http\Request;
use BullitEngine\Component\Http\Response;
use BullitEngine\Component\Routing\Context;
use BullitEngine\Component\Routing\Exception\ResourceNotFoundException;
use BullitEngine\Component\Routing\Route;
use BullitEngine\Component\Routing\Router;
use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase
{
    public function testAdd()
    {
        $foo = function() {
        };

        $router = new Router();
        $router->add('foo', new Route('/foo', $foo));

        $this->assertArrayHasKey('foo', $router->all());
    }

    public function testGet()
    {
        $foo = function() {
        };

        $router = new Router();
        $router->add('foo', new Route('/foo', $foo));

        $this->assertEquals(new Route('/foo', $foo), $router->get('foo'));
    }

    public function testCount()
    {
        $foo = function() {
        };

        $router = new Router();
        $router->add('foo', new Route('/foo', $foo));
        $router->add('foo2', new Route('/foo2', $foo));
        $router->add('foo3', new Route('/foo3', $foo));

        $this->assertCount(3, $router);
    }

    public function testIterator()
    {
        $foo = function() {
        };

        $router = new Router();
        $router->add('foo', new Route('/foo', $foo));
        $router->add('foo2', new Route('/foo2', $foo));

        foreach ($router as $name => $route) {
            $this->assertInstanceOf('BullitEngine\Component\Routing\Route', $route);
        }
    }

    public function testMatchRouteWithNoParameters()
    {
        $foo = function() {
        };

        $server = [
            'REQUEST_URI'    => '/foo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);

        $router = new Router();
        $router->add('foo', new Route('/foo', $foo));

        $this->assertEquals([
            'name'   => 'foo',
            'action' => $foo
        ], $router->match(Context::createFromRequest($request))
                  ->all());
    }

    public function testMatchRouteWithRequiredParameters()
    {
        $foo = function() {
        };

        $server = [
            'REQUEST_URI'    => '/foo/value1/value2',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo/value1/value2',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);

        $router = new Router();
        $router->add('foo', new Route('/foo/{var1}/{var2}', $foo));

        $expected = [
            'name'   => 'foo',
            'action' => $foo,
            'var1'   => 'value1',
            'var2'   => 'value2'
        ];

        $this->assertEquals($expected, $router->match(Context::createFromRequest($request))
                                              ->all());
    }

    public function testMatchRouteWithOptionalParameters()
    {
        $foo = function() {
        };

        $server = [
            'REQUEST_URI'    => '/foo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo',
            'REQUEST_METHOD' => 'GET'
        ];


        $request = new Request([], [], [], [], $server);
        $router  = new Router();
        $router->add('foo', new Route('/foo/{var1?}/{var2?}', $foo));

        $expected = [ 'name' => 'foo', 'action' => $foo ];

        $this->assertEquals($expected, $router->match(Context::createFromRequest($request))
                                              ->all());
    }

    public function testMatchBadMethod()
    {
        $this->expectException(ResourceNotFoundException::class);

        $foo = function() {
        };

        $server = [
            'REQUEST_URI'    => '/foo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);
        $router  = new Router();
        $router->add('foo', new Route('/foo', $foo, [ 'POST' ]));

        $router->match(Context::createFromRequest($request));
    }

    public function testMatchBadPath()
    {
        $this->expectException(ResourceNotFoundException::class);

        $foo = function() {
        };

        $server = [
            'REQUEST_URI'    => '/fooooo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/fooooo',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);
        $router  = new Router();
        $router->add('foo', new Route('/foo', $foo));

        $router->match(Context::createFromRequest($request));
    }

    public function testMatchMissingParameter()
    {
        $this->expectException(ResourceNotFoundException::class);

        $foo = function() {
        };

        $server = [
            'REQUEST_URI'    => '/foo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);
        $router  = new Router();
        $router->add('foo', new Route('/foo/{var}', $foo));

        $router->match(Context::createFromRequest($request));
    }

    public function testDispatchWithFunctionAsAction()
    {
        $server = [
            'REQUEST_URI'    => '/foo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);

        $action = function() {
            return new Response('OK');
        };

        $router = new Router();
        $router->add('foo', new Route('/foo', $action));

        $response = $router->dispatch($request);

        $this->assertEquals(new Response('OK'), $response);
    }

    public function testDispatchWithMethodAsAction()
    {
        $server = [
            'REQUEST_URI'    => '/foo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);

        $obj = $this->getMockBuilder('MyClass')
            ->setMethods([ 'action' ])
            ->getMock();
        $obj->expects($this->any())
            ->method('action')
            ->willReturn(new response('OK'));

        $router = new Router();
        $router->add('foo', new Route('/foo', [ $obj, 'action' ]));

        $response = $router->dispatch($request);

        $this->assertEquals(new Response('OK'), $response);
    }

    public function testDispatchWithClassAsAction()
    {
        $server = [
            'REQUEST_URI'    => '/foo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);

        $db = $this->getMockBuilder('BullitEngine\Component\Database\PdoAdapter')
                   ->disableOriginalConstructor()
                   ->getMock();

        $container = $this->getMockBuilder(Container::class)
                          ->getMock();
        $container->expects($this->any())
                  ->method('getParameter')
                  ->willThrowException(new ContainerException(''));
        $container->expects($this->any())
                  ->method('instances')
                  ->willReturn([ 'database' => $db ]);
        $container->expects($this->any())
                  ->method('service')
                  ->willReturnMap([
                      [ 'BullitEngine\Component\Http\Request', $request ],
                      [ 'BullitEngine\Component\Routing\Tests\A', new A() ],
                      [ 'BullitEngine\Component\Routing\Tests\B', new B() ]
                  ]);
        $container->expects($this->any())
                  ->method('exists')
                  ->willReturnCallback(function($p) {
                      return in_array($p, [ 'BullitEngine\Component\Routing\Tests\A' ]);
                  });

        $container->expects($this->any())
            ->method('alias')
            ->willReturnMap([
                [ 'BullitEngine\Component\Routing\Tests\B', 'BullitEngine\Component\Routing\Tests\B' ]
            ]);

        $container->expects($this->any())
                  ->method('isAlias')
                  ->willReturnCallback(function($p) {
                      return in_array($p, [ 'BullitEngine\Component\Http\Request', 'BullitEngine\Component\Routing\Tests\B' ]);
                  });

        $router = new Router($container);
        $router->add('foo', new Route('/foo', 'BullitEngine\Component\Routing\Tests\Foo@bar'));

        $response = $router->dispatch($request);

        $this->assertEquals(new Response('OK'), $response);
    }

    public function testDispatchWithRouteHasParameters()
    {
        $server = [
            'REQUEST_URI'    => '/foo/value1',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo/value1',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);

        $action = function($var1, $var2 = 'value2') {
            return new Response($var1.$var2);
        };

        $router = new Router();
        $router->add('foo', new Route('/foo/{var1}/{var2?}', $action));

        $response = $router->dispatch($request);

        $this->assertEquals(new Response('value1'.'value2'), $response);
    }

    public function testDispatchRouteNotFound()
    {
        $this->expectException(ResourceNotFoundException::class);

        $server = [
            'REQUEST_URI'    => '/fooooo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/fooooo',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);

        $action = function() {
            return new Response('OK');
        };

        $router = new Router();
        $router->add('foo', new Route('/foo', $action));

        $router->dispatch($request);
    }

    public function testDispatchThrowingException()
    {
        $this->expectException(\Exception::class);

        $server = [
            'REQUEST_URI'    => '/foo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);

        $action = function() {
            throw new \Exception();
        };

        $router = new Router();
        $router->add('foo', new Route('/foo', $action));

        $router->dispatch($request);
    }

    public function testDispatchWithMatchThrowingAnLogicException()
    {
        $this->expectException(\LogicException::class);

        $server = [
            'REQUEST_URI'    => '/foo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);
        $action  = function() {
            return new Response('OK');
        };

        $router = $this->getMockBuilder(Router::class)
                       ->setMethods([ 'match' ])
                       ->getMock();

        $router->expects($this->any())
               ->method('match')
               ->will($this->throwException(new \ReflectionException()));

        /** @noinspection PhpUndefinedMethodInspection */
        $router->add('foo', new Route('/foo', $action));
        /** @noinspection PhpUndefinedMethodInspection */
        $router->dispatch($request);
    }

    public function testDispatchBadAction()
    {
        $this->expectException(\LogicException::class);

        $server = [
            'REQUEST_URI'    => '/foo',
            'QUERY_STRING'   => '',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);

        $action = function($var) {
            return new Response($var);
        };

        $router = new Router();
        $router->add('foo', new Route('/foo/{var?}', $action));

        $router->dispatch($request);
    }
}

class Foo
{
    public function bar(/** @noinspection PhpUnusedParameterInspection */
        Request $request, DatabaseAdapterInterface $db, A $a, B $b, Response $notexists = null)
    {
        return new Response('OK');
    }
}

class A
{
}

class B
{
}
