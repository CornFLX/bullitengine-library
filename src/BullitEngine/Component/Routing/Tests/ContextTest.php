<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Routing\Tests;


use BullitEngine\Component\Http\Request;
use BullitEngine\Component\Routing\Context;
use PHPUnit\Framework\TestCase;

class ContextTest extends TestCase
{

    public function testConstructor()
    {
        $this->testCreateFromRequest();
    }

    public function testCreateFromRequest()
    {
        $server = [
            'REQUEST_URI'    => '/front.php/foo/bar?var=10',
            'QUERY_STRING'   => 'var=10',
            'SCRIPT_NAME'    => '/front.php',
            'PATH_INFO'      => '/foo/bar',
            'REQUEST_METHOD' => 'GET'
        ];

        $request = new Request([], [], [], [], $server);
        $context = Context::createFromRequest($request);

        $this->assertEquals('GET', $context->method());
        $this->assertEquals('/front.php', $context->baseurl());
        $this->assertEquals('/foo/bar', $context->path());
        $this->assertEquals('var=10', $context->querystring());
    }


}
