<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Routing;

use BullitEngine\Component\Container\Container;
use BullitEngine\Component\Container\Exception\ContainerException;
use BullitEngine\Component\Http\Request;
use BullitEngine\Component\Http\Response;
use BullitEngine\Component\Routing\Exception\ResourceNotFoundException;
use Traversable;

/**
 * Class Router
 *
 * @package BullitEngine\Component\Routing
 */
class Router implements \IteratorAggregate, \Countable
{
    /**
     * The route's collection.
     *
     * @var Route[]
     */
    protected $routes;

    /** @var Container */
    protected $container;

    /**
     * Constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container = null)
    {
        $this->container = $container ? $container : new Container();
    }

    /**
     * Dispatch the request to the application.
     *
     * @param Request $request
     *
     * @return Response
     * @throws ContainerException
     * @throws ResourceNotFoundException
     * @throws \LogicException
     * @throws \RuntimeException
     */
    public function dispatch(Request $request) : Response
    {
        try {
            $context = Context::createFromRequest($request);
            $attrs   = $this->match($context);
            $action  = $attrs->get('action');

            if (is_array($action)) {
                $func = new \ReflectionMethod($action[0], $action[1]);
            } else {
                $func = new \ReflectionFunction($action);
            }

            $ordered_parameters = [];

            foreach ($func->getParameters() as $parameter) {
                try {
                    $value = $this->container->getParameter(str_replace('_', '.', $parameter->getName()));
                } catch (ContainerException $e) {
                    $value = null;
                }

                if (null === $value) {
                    if (($class = $parameter->getClass()) !== null) {
                        $class = $class->name;
                        if ($request instanceof $class) {
                            $value = $request;
                        } else {
                            $value = $this->resolveDependency($parameter);
                        }
                    } else {
                        $value = $attrs->get($parameter->getName());
                    }
                }

                if (null === $value) {
                    $value = $parameter->getDefaultValue();
                }

                $ordered_parameters[] = $value;
            }

            $response = call_user_func_array($action, $ordered_parameters);
        } catch (\ReflectionException $e) {
            if (!isset($attrs)) {
                $attrs = new AttributeContainer([ 'name' => 'unknown' ]);
            }
            throw new \LogicException('Bad action defined for the route "'.$attrs->get('name').'" An optionnal parameter has no default value.');
        }

        return $response;
    }

    /**
     * Tries to match a Context with the routes.
     *
     * @param Context $context
     *
     * @return AttributeContainer
     * @throws ResourceNotFoundException
     * @throw ResourceNotFoundException
     */
    public function match(Context $context) : AttributeContainer
    {
        foreach ($this->routes as $name => $route) {
            if (count($route->methods()) > 0 && !in_array($context->method(), $route->methods())) {
                continue;
            }

            // Convert path to pattern
            $pattern = '#^'.$route->path().'$#';
            $pattern = preg_replace('/(\/([^{\/]*){([^\/\?]+)\??})/', '(?:/(?<$3>[^\/\?]*))?', $pattern, -1, $count);

            // Test pattern
            if (0 === preg_match($pattern, $context->path(), $matches)) {
                continue;
            }

            // Check parameters
            $attributes      = [];
            $list_parameters = explode(' ', trim(preg_replace('/([^{]*{)([^}\/]+)}/', '$2 ', $route->path(), -1, $count)));
            if ($count > 0) {
                foreach ($list_parameters as $parameter) {
                    $optionnal = strrpos($parameter, '?', -1) !== false;
                    if ($optionnal) {
                        $parameter = substr($parameter, 0, -1);
                    }

                    if (isset($matches[$parameter])) {
                        $attributes[$parameter] = $matches[$parameter];
                    } else {
                        if ($optionnal) {
                            continue;
                        } else {
                            continue 2; // missing parameter, continue to try next routes
                        }
                    }
                }
            }

            return $this->attributes($route, $name, $attributes);
        }

        throw new ResourceNotFoundException('No route found for path "'.$context->path().'".');
    }

    /**
     * @param Route  $route
     * @param string $route_name
     * @param array  $attributes
     *
     * @return AttributeContainer
     */
    private function attributes(Route $route, string $route_name, array $attributes) : AttributeContainer
    {
        $attributes = new AttributeContainer($attributes);
        $attributes->set('name', $route_name);
        $attributes->set('action', $route->action());

        return $attributes;
    }

    /**
     * @param \ReflectionParameter $reflector
     *
     * @return mixed
     * @throws ContainerException
     * @throws \RuntimeException
     */
    private function resolveDependency(\ReflectionParameter $reflector)
    {
        $class = $reflector->getClass()->name;

        if ($this->container->exists($class)) {
            return $this->container->service($class);
        }

        if ($this->container->isAlias($class)) {
            return $this->container->service($this->container->alias($class));
        }

        foreach ($this->container->instances() as $key => $value) {
            if ($value instanceof $class) {
                return $value;
            }
        }

        return null;
    }

    /**
     * Adds a route into the collection.
     * If a route with same name exists, it will be overwritten.
     *
     * @param string $name The route name
     * @param Route  $route The route
     */
    public function add(string $name, Route $route) : void
    {
        $this->routes[$name] = $route;
    }

    /**
     * Returns a route by its name.
     *
     * @param string $name
     *
     * @return Route|null
     */
    public function get(string $name) : Route
    {
        return isset($this->routes[$name]) ? $this->routes[$name] : null;
    }

    /**
     * Return all routes.
     *
     * @return Route[]
     */
    public function all() : array
    {
        return $this->routes;
    }

    /**
     * Retrieve an external iterator.
     *
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     *
     * @return Traversable An instance of an object implementing Iterator or Traversable
     */
    public function getIterator() : Traversable
    {
        return new \ArrayIterator($this->routes);
    }

    /**
     * Count elements of an object.
     *
     * @link http://php.net/manual/en/countable.count.php
     *
     * @return int The custom count as an integer.
     */
    public function count() : int
    {
        return count($this->routes);
    }
}

