<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Routing;

use Traversable;

/**
 * Class AttributeContainer
 *
 * @package BullitEngine\Component\Routing
 */
class AttributeContainer implements \IteratorAggregate, \Countable
{
    /**
     * @var array
     */
    private $attributes;

    /**
     * Constructor.
     *
     * @param array $attributes An array of attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    /**
     * Return an attribute by its name.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function get(string $name)
    {
        return isset($this->attributes[$name]) ? $this->attributes[$name] : null;
    }

    /**
     * Return all attributes.
     *
     * @return array
     */
    public function all() : array
    {
        return $this->attributes;
    }

    /**
     * Set an attribute.
     *
     * @param string $name
     * @param mixed  $value
     */
    public function set(string $name, $value) : void
    {
        $this->attributes[$name] = $value;
    }

    /**
     * Retrieve an external iterator.
     *
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     *
     * @return Traversable An instance of an object implementing Iterator or Traversable.
     */
    public function getIterator() : Traversable
    {
        return new \ArrayIterator($this->attributes);
    }

    /**
     * Count elements of an object.
     *
     * @link http://php.net/manual/en/countable.count.php
     *
     * @return int The custom count as an integer.
     */
    public function count() : int
    {
        return count($this->attributes);
    }
}

