<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Routing;

use BullitEngine\Component\Http\Request;

/**
 * Class Context
 *
 * @package BullitEngine\Component\Routing
 */
class Context
{
    /**
     * The HTTP method.
     *
     * @var string
     */
    protected $method;

    /**
     * The base url.
     *
     * @var string
     */
    protected $baseurl;

    /**
     * The path.
     *
     * @var string
     */
    protected $path;

    /**
     * The query string.
     *
     * @var string
     */
    protected $querystring;

    /**
     * Constructor.
     *
     * @param string $baseurl
     * @param string $path
     * @param string $querystring
     * @param string $method
     */
    public function __construct(string $baseurl = '', string $path = '/', string $querystring = '', string $method = 'GET')
    {
        $this->setBaseurl($baseurl);
        $this->setPath($path);
        $this->setQuerystring($querystring);
        $this->setMethod($method);
    }

    /**
     * Instanciate a Context with a Request object.
     *
     * @param Request $request
     *
     * @return Context
     */
    static public function createFromRequest(Request $request) : Context
    {
        return new self($request->baseurl(), $request->pathInfo(), $request->server()
                                                                           ->get('QUERY_STRING'), $request->method());
    }

    /**
     * @return string
     */
    public function method() : string
    {
        return $this->method;
    }

    /**
     * @param string $method
     */
    public function setMethod(string $method) : void
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function baseurl() : string
    {
        return $this->baseurl;
    }

    /**
     * @param string $baseurl
     */
    public function setBaseurl(string $baseurl) : void
    {
        $this->baseurl = $baseurl;
    }

    /**
     * @return string
     */
    public function path() : string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path) : void
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function querystring() : string
    {
        return $this->querystring;
    }

    /**
     * @param string $querystring
     */
    public function setQuerystring(string $querystring) : void
    {
        $this->querystring = $querystring;
    }
}

