<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Http\Tests;


use BullitEngine\Component\Http\Response;
use PHPUnit\Framework\TestCase;

class ResponseTest extends TestCase
{

    public function testConstructor()
    {
        $response = new Response();
        $this->assertEquals('', $response->content());
        $this->assertEquals(200, $response->statusCode());

        $response = new Response('foo', 403);
        $this->assertEquals('foo', $response->content());
        $this->assertEquals(403, $response->statusCode());

        $response = new Response('bar', 301, [ 'Content-Type' => 'text/html' ]);
        $this->assertEquals('bar', $response->content());
        $this->assertEquals(301, $response->statusCode());
        $this->assertEquals('text/html', $response->headers()['Content-Type']);
    }

    public function testSetStatusCodeInvalid()
    {
        $this->expectException(\InvalidArgumentException::class);

        $response = new Response();
        $response->setStatusCode(9999);
    }

    public function testSetStatusCode()
    {
        $response = new Response();
        $response->setStatusCode(404);
        $this->assertEquals(404, $response->statusCode());
    }

    /**
     * @runInSeparateProcess
     */
    public function testSendHeaders()
    {
        $response = new Response('foo', 200);
        $response->sendHeaders();

        $this->assertObjectHasAttribute('headers', $response);
        $this->assertEquals('text/html; charset=utf-8', $response->headers()['Content-Type']);
    }

    public function testNotResendHeaders()
    {
        $response = new Response('foo', 200);
        $response->sendHeaders();

        $this->assertArrayNotHasKey('Content-Type', $response->headers());
    }

    public function testSendContent()
    {
        $this->expectOutputString('foo');

        $response = new Response('foo', 200);
        $response->sendContent();
    }

    /**
     * @runInSeparateProcess
     */
    public function testSend()
    {
        $this->expectOutputString('foo');

        $response = new Response('foo', 200);
        $response->send();

        $this->assertObjectHasAttribute('headers', $response);
        $this->assertEquals('text/html; charset=utf-8', $response->headers()['Content-Type']);
    }

    public function testSetStatusText()
    {
        $response = new Response('foo', 200);
        $response->setStatusText('OK');

        $this->assertEquals('OK', $response->statusText());
    }

    public function testSetCharset()
    {
        $response = new Response('foo', 200);
        $response->setCharset('UTF-8');

        $this->assertEquals('UTF-8', $response->charset());
    }
}
