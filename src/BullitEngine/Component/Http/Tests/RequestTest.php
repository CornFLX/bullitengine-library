<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Http\Tests;

use BullitEngine\Component\Http\Request;
use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{

    public function testConstructor()
    {
        $this->testInitialize();
    }

    public function testInitialize()
    {
        $request = new Request();

        $request->initialize([ 'foo' => 'bar' ]);
        $this->assertEquals('bar', $request->query()
                                           ->get('foo'));

        $request->initialize([], [ 'foo' => 'bar' ]);
        $this->assertEquals('bar', $request->request()
                                           ->get('foo'));

        $request->initialize([], [], [ 'foo' => 'bar' ]);
        $this->assertEquals('bar', $request->cookies()
                                           ->get('foo'));

        $request->initialize([], [], [], [ 'foo' => 'bar' ]);
        $this->assertEquals('bar', $request->files()
                                           ->get('foo'));

        $request->initialize([], [], [], [], [ 'foo' => 'bar' ]);
        $this->assertEquals('bar', $request->server()
                                           ->get('foo'));
    }

    public function testCreateFromGlobal()
    {
        $_GET['fooget']       = 'barget';
        $_POST['foopost']     = 'barpost';
        $_COOKIE['foocookie'] = 'barcookie';
        $_FILES['foofiles']   = 'barfiles';
        $_SERVER['fooserver'] = 'barserver';

        $request = Request::createFromGlobals();

        $this->assertEquals('barget', $request->query()
                                              ->get('fooget'));
        $this->assertEquals('barpost', $request->request()
                                               ->get('foopost'));
        $this->assertEquals('barcookie', $request->cookies()
                                                 ->get('foocookie'));
        $this->assertEquals('barfiles', $request->files()
                                                ->get('foofiles'));
        $this->assertEquals('barserver', $request->server()
                                                 ->get('fooserver'));
    }

    public function testPathInfoIfNotInServer()
    {
        $server = [
            'REQUEST_URI' => '/front.php/foo/bar?var=10',
            'SCRIPT_NAME' => '/front.php'
        ];

        $request = new Request([], [], [], [], $server);
        $this->assertEquals('/foo/bar', $request->pathInfo());

        $server  = [
            'REQUEST_URI' => '/foo/bar?var=10',
            'SCRIPT_NAME' => '/front.php'
        ];
        $request = new Request([], [], [], [], $server);
        $this->assertEquals('/foo/bar', $request->pathInfo());
    }

    public function testPathInfo()
    {
        $server = [
            'REQUEST_URI' => '/front.php/foo/bar?var=10',
            'SCRIPT_NAME' => '/front.php',
            'PATH_INFO'   => '/foo/bar'
        ];

        $request = new Request([], [], [], [], $server);
        $this->assertEquals('/foo/bar', $request->pathInfo());
    }

    public function testBaseUrl()
    {
        $server = [
            'REQUEST_URI' => '/front.php/foo/bar?var=10',
            'SCRIPT_NAME' => '/front.php'
        ];

        $request = new Request([], [], [], [], $server);
        $this->assertEquals('/front.php', $request->baseurl());

        $server = [
            'REQUEST_URI' => '/foo/bar?var=10',
            'SCRIPT_NAME' => '/front.php'
        ];

        $request = new Request([], [], [], [], $server);
        $this->assertEquals('', $request->baseurl());
    }

    public function testMethod()
    {
        $server = [
            'REQUEST_METHOD' => 'POST'
        ];

        $request = new Request([], [], [], [], $server);
        $this->assertEquals('POST', $request->method());
    }
}
