<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Http\Tests;


use BullitEngine\Component\Http\ParameterContainer;
use PHPUnit\Framework\TestCase;

class ParameterContainerTest extends TestCase
{
    public function testGet()
    {
        $parameters = new ParameterContainer([ 'foo' => 'bar' ]);
        $this->assertEquals('bar', $parameters->get('foo'));
        $this->assertEquals(null, $parameters->get('foo2'));
        $this->assertEquals('bar3', $parameters->get('foo3', 'bar3'));
    }

    public function testSet()
    {
        $parameters = new ParameterContainer();
        $parameters->set('foo', 'bar');
        $this->assertEquals('bar', $parameters->get('foo'));
    }

    public function testAll()
    {
        $parameters = new ParameterContainer([
            'foo'  => 'bar',
            'foo2' => 'bar2',
            'foo3' => 'bar3'
        ]);
        $this->assertEquals('bar', $parameters->all()['foo']);
        $this->assertEquals('bar2', $parameters->all()['foo2']);
        $this->assertEquals('bar3', $parameters->all()['foo3']);
    }

    public function testGetIterator()
    {
        $parameters = new ParameterContainer([ 'foo' => 'bar' ]);
        foreach ($parameters as $name => $value) {
            $this->assertEquals('foo', $name);
            $this->assertEquals('bar', $value);
        }
    }

    public function testCount()
    {
        $parameters = new ParameterContainer([
            'foo'  => 'bar',
            'foo2' => 'bar2',
            'foo3' => 'bar3'
        ]);
        $this->assertEquals(3, count($parameters));
    }
}
