<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Http;

/**
 * Class Request
 *
 * @package BullitEngine\Component\Http
 */
class Request
{
    /**
     * Query string parameters ($_GET).
     *
     * @var ParameterContainer
     */
    protected $query;
    /**
     * Request body parameters ($_POST).
     *
     * @var ParameterContainer
     */
    protected $request;
    /**
     * Cookies ($_COOKIE).
     *
     * @var ParameterContainer
     */
    protected $cookies;
    /**
     * Uploaded files ($_FILES).
     *
     * @var ParameterContainer
     */
    protected $files;
    /**
     * Server and execution environment parameters ($_SERVER).
     *
     * @var ParameterContainer
     */
    protected $server;

    /**
     * Constructor.
     *
     * @param array $query The GET parameters
     * @param array $request The POST parameters
     * @param array $cookies The COOKIE parameters
     * @param array $files The FILES parameters
     * @param array $server The SERVER parameters
     */
    public function __construct(array $query = [], array $request = [], array $cookies = [], array $files = [], array $server = [])
    {
        $this->initialize($query, $request, $cookies, $files, $server);
    }

    /**
     * Sets the parameters for this request.
     *
     * @param array $query The GET parameters
     * @param array $request The POST parameters
     * @param array $cookies The COOKIE parameters
     * @param array $files The FILES parameters
     * @param array $server The SERVER parameters
     */
    public function initialize(array $query = [], array $request = [], array $cookies = [], array $files = [], array $server = [])
    {
        $this->query   = new ParameterContainer($query);
        $this->request = new ParameterContainer($request);
        $this->cookies = new ParameterContainer($cookies);
        $this->files   = new ParameterContainer($files);
        $this->server  = new ParameterContainer($server);
    }

    /**
     * Create a new request with PHP's super globals.
     *
     * @return Request
     */
    static public function createFromGlobals() : Request
    {
        return new self($_GET, $_POST, $_COOKIE, $_FILES, $_SERVER);
    }

    /**
     * Returns the path relative to the executed script.
     * Example (request is instantiated from /foo):
     *   http://www.example.org/foo         return ''
     *   http://www.example.org/foo/bar     return '/bar'
     *   http://www.example.org/foo/bar?var return '/bar'
     */
    public function pathInfo() : string
    {
        if ($path_info = $this->server()
                              ->get('PATH_INFO')
        ) {
            return $path_info;
        }

        $request_uri = $this->server()
                            ->get('REQUEST_URI');
        $script_name = '/'.basename($this->server()
                                         ->get('SCRIPT_NAME'));

        if ($pos = strpos($request_uri, '?')) {
            $request_uri = substr($request_uri, 0, $pos);
        }

        $path_info = $request_uri;

        if (0 === strpos($request_uri, $script_name)) {
            $path_info = substr($request_uri, strlen($script_name));
        }

        return $path_info;
    }

    /**
     * @return ParameterContainer
     */
    public function server() : ParameterContainer
    {
        return $this->server;
    }

    /**
     * Returns the request's method.
     *
     * @return string
     */
    public function method() : string
    {
        return $this->server()
                    ->get('REQUEST_METHOD');
    }

    /**
     * Returns the baseurl.
     * Ex :
     *      http://www.example.org/foo           return ''
     *      http://www.example.org/file.php/foo  return '/file.php'
     *
     * @return string
     */
    public function baseurl() : string
    {
        return 0 === strpos($this->server()->get('REQUEST_URI'),
            $this->server()->get('SCRIPT_NAME')) ? $this->server()->get('SCRIPT_NAME') : '';
    }

    /**
     * @return ParameterContainer
     */
    public function query() : ParameterContainer
    {
        return $this->query;
    }

    /**
     * @return ParameterContainer
     */
    public function request() : ParameterContainer
    {
        return $this->request;
    }

    /**
     * @return ParameterContainer
     */
    public function cookies() : ParameterContainer
    {
        return $this->cookies;
    }

    /**
     * @return ParameterContainer
     */
    public function files() : ParameterContainer
    {
        return $this->files;
    }
}

