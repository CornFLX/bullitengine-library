<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Http;

use Traversable;

/**
 * Class ParameterContainer
 *
 * @package BullitEngine\Component\Http
 */
class ParameterContainer implements \IteratorAggregate, \Countable
{
    /**
     * @var array
     */
    private $parameters;

    /**
     * Constructor.
     *
     * @param array $parameters An array of parameters
     */
    public function __construct(array $parameters = [])
    {
        $this->parameters = $parameters;
    }

    /**
     * Return a parameter by its name.
     *
     * @param string $name
     * @param mixed  $default
     *
     * @return mixed
     */
    public function get(string $name, $default = null)
    {
        return isset($this->parameters[$name]) ? $this->parameters[$name] : $default;
    }

    /**
     * Return all parameters.
     *
     * @return array
     */
    public function all() : array
    {
        return $this->parameters;
    }

    /**
     * Set a parameter.
     *
     * @param string $name
     * @param mixed  $value
     */
    public function set(string $name, $value) : void
    {
        $this->parameters[$name] = $value;
    }

    /**
     * Retrieve an external iterator.
     *
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     *
     * @return Traversable An instance of an object implementing Iterator or Traversable.
     */
    public function getIterator() : Traversable
    {
        return new \ArrayIterator($this->parameters);
    }

    /**
     * Count elements of an object.
     *
     * @link http://php.net/manual/en/countable.count.php
     *
     * @return int The custom count as an integer.
     */
    public function count() : int
    {
        return count($this->parameters);
    }
}

