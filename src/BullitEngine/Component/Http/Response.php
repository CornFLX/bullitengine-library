<?php

/*
 * This file is part of the BullitEngine package.
 *
 * (c) CornFLX <cornflx@aleksya.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BullitEngine\Component\Http;

/**
 * Class Response
 *
 * @package BullitEngine\Component\Http
 */
class Response
{
    const HTTP_CONTINUE            = 100;
    const HTTP_SWITCHING_PROTOCOLS = 101;
    const HTTP_PROCESSING          = 102;

    const HTTP_OK                            = 200;
    const HTTP_CREATED                       = 201;
    const HTTP_ACCEPTED                      = 202;
    const HTTP_NON_AUTHORITATIVE_INFORMATION = 203;
    const HTTP_NO_CONTENT                    = 204;
    const HTTP_RESET_CONTENT                 = 205;
    const HTTP_PARTIAL_CONTENT               = 206;
    const HTTP_MULTI_STATUS                  = 207;
    const HTTP_ALREADY_REPORTED              = 208;
    const HTTP_IM_USED                       = 226;

    const HTTP_MULTIPLE_CHOICES   = 300;
    const HTTP_MOVED_PERMANENTLY  = 301;
    const HTTP_FOUND              = 302;
    const HTTP_SEE_OTHER          = 303;
    const HTTP_NOT_MODIFIED       = 304;
    const HTTP_USE_PROXY          = 305;
    const HTTP_TEMPORARY_REDIRECT = 307;
    const HTTP_PERMANENT_REDIRECT = 308;

    const HTTP_BAD_REQUEST                     = 400;
    const HTTP_UNAUTHORIZED                    = 401;
    const HTTP_PAYMENT_REQUIRED                = 402;
    const HTTP_FORBIDDEN                       = 403;
    const HTTP_NOT_FOUND                       = 404;
    const HTTP_METHOD_NOT_ALLOWED              = 405;
    const HTTP_NOT_ACCEPTABLE                  = 406;
    const HTTP_PROXY_AUTHENTIFICATION_REQUIRED = 407;
    const HTTP_REQUEST_TIMEOUT                 = 408;
    const HTTP_CONFLICT                        = 409;
    const HTTP_GONE                            = 410;
    const HTTP_LENGTH_REQUIRED                 = 411;
    const HTTP_PRECONDITION_FAILED             = 412;
    const HTTP_PAYLOAD_TOO_LARGE               = 413;
    const HTTP_URI_TOO_LONG                    = 414;
    const HTTP_UNSUPPORTED_MEDIA_TYPE          = 415;
    const HTTP_RANGE_UNSATISFIABLE             = 416;
    const HTTP_EXPECTATION_FAILED              = 417;
    const HTTP_I_AM_A_TEAPOT                   = 418;
    const HTTP_UNPROCESSABLE_ENTITY            = 422;
    const HTTP_LOCKED                          = 423;
    const HTTP_FAILED_DEPENDENCY               = 424;
    const HTTP_UPGRADE_REQUIRED                = 426;
    const HTTP_PRECONDITION_REQUIRED           = 428;
    const HTTP_TOO_MANY_REQUESTS               = 429;
    const HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;

    const HTTP_INTERNAL_SERVER_ERROR   = 500;
    const HTTP_NOT_IMPLEMENTED         = 501;
    const HTTP_BAD_GATEWAY             = 502;
    const HTTP_SERVICE_UNAVAILABLE     = 503;
    const HTTP_GATEWAY_TIMEOUT         = 504;
    const HTTP_VERSION_NOT_SUPPORTED   = 505;
    const HTTP_VARIANT_ALSO_NEGOCIATE  = 506;
    const HTTP_INSUFFICIENT_STORAGE    = 507;
    const HTTP_LOOP_DETECTED           = 508;
    const HTTP_BANDWITH_LIMIT_EXCEEDED = 509;
    const HTTP_NOT_EXTENDED            = 510;

    /**
     * Translation map HTTP status codes => status text.
     *
     * @var array
     */
    static public $status
        = [
            100 => 'Continue',
            101 => 'Switching',
            102 => 'Processing',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative',
            204 => 'No',
            205 => 'Reset',
            206 => 'Partial',
            207 => 'Multi-Status',
            208 => 'Already',
            226 => 'IM',
            300 => 'Multiple',
            301 => 'Moved',
            302 => 'Found',
            303 => 'See',
            304 => 'Not',
            305 => 'Use',
            306 => '(Unused)',
            307 => 'Temporary',
            308 => 'Permanent',
            400 => 'Bad',
            401 => 'Unauthorized',
            402 => 'Payment',
            403 => 'Forbidden',
            404 => 'Not',
            405 => 'Method',
            406 => 'Not',
            407 => 'Proxy',
            408 => 'Request',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length',
            412 => 'Precondition',
            413 => 'Payload',
            414 => 'URI',
            415 => 'Unsupported',
            416 => 'Range',
            417 => 'Expectation',
            421 => 'Misdirected',
            422 => 'Unprocessable',
            423 => 'Locked',
            424 => 'Failed',
            426 => 'Upgrade',
            428 => 'Precondition',
            429 => 'Too',
            431 => 'Request',
            500 => 'Internal',
            501 => 'Not',
            502 => 'Bad',
            503 => 'Service',
            504 => 'Gateway',
            505 => 'HTTP',
            506 => 'Variant',
            507 => 'Insufficient',
            508 => 'Loop',
            510 => 'Not',
            511 => 'Network'
        ];
    /**
     * The response content.
     *
     * @var string
     */
    private $content;
    /**
     * The response status code.
     *
     * @var int
     */
    private $status_code;
    /**
     * The response status text.
     *
     * @var string
     */
    private $status_text;
    /**
     * The response headers.
     *
     * @var array
     */
    private $headers;
    /**
     * The response charset.
     *
     * @var string
     */
    private $charset;

    /**
     * Constructor.
     *
     * @param string $content The response content
     * @param int    $status_code The response status
     * @param array  $headers The response headers
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(string $content = '', int $status_code = 200, array $headers = [])
    {
        $this->setContent($content);
        $this->setStatusCode($status_code);
        $this->setHeaders($headers);
        $this->setCharset('');
    }

    /**
     * @return string
     */
    public function content() : string
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content) : void
    {
        $this->content = $content;
    }

    /**
     * @return array
     */
    public function headers() : array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     */
    public function setHeaders(array $headers) : void
    {
        $this->headers = $headers;
    }

    /**
     * Send response.
     */
    public function send() : void
    {
        $this->sendHeaders();
        $this->sendContent();
    }

    /**
     * Send response headers.
     */
    public function sendHeaders() : void
    {
        if (headers_sent()) {
            return;
        }

        // status line
        header('HTTP/1.1 '.$this->statusCode().' '.$this->statusText(), true, $this->statusCode());

        // default Content-Type is "text/html"
        if (!isset($this->headers['Content-Type'])) {
            $this->headers['Content-Type'] = 'text/html';
        }

        // add charset for text/* mime type
        $charset = $this->charset() ? $this->charset() : 'utf-8';
        if (0 === stripos($this->headers['Content-Type'], 'text/') && false === stripos($this->headers['Content-Type'], 'charset')) {
            $this->headers['Content-Type'] .= '; charset='.$charset;
        }

        // send
        foreach ($this->headers as $name => $value) {
            header($name.': '.$value, true);
        }
    }

    /**
     * @return int
     */
    public function statusCode() : int
    {
        return $this->status_code;
    }

    /**
     * @param $status_code
     *
     * @throws \InvalidArgumentException
     */
    public function setStatusCode(int $status_code) : void
    {
        if (!isset(self::$status[$status_code])) {
            throw new \InvalidArgumentException('Response status code "'.$status_code.'" is invalid.');
        }
        $this->status_code = $status_code;
        $this->status_text = self::$status[$status_code];
    }

    /**
     * @return string
     */
    public function statusText() : string
    {
        return $this->status_text;
    }

    /**
     * @param string $status_text
     */
    public function setStatusText(string $status_text) : void
    {
        $this->status_text = $status_text;
    }

    /**
     * @return string
     */
    public function charset() : string
    {
        return $this->charset;
    }

    /**
     * @param string $charset
     */
    public function setCharset(string $charset) : void
    {
        $this->charset = $charset;
    }

    /**
     * Send response content.
     */
    public function sendContent() : void
    {
        print $this->content;
    }
}

