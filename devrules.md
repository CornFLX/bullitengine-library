Règles de developpement
==========================


Git
-----

### Nommage des branches
`ID-issue/description`  
*ex: BE-72/module_logger*



### Message de commit:
`TYPE (ID-issue) message`  

Valeurs possible pour TYPE :

  - FEAT: dernier commit sur une branche feature  
  - WIP: work in progress    
  - DOC: écriture de doc
  - FIX: correctif
  
*ex: FEAT (BE-72) Le super message*


Code source
-------------

### Formattage
Télécharger le fichier de configuration PHPStorm disponible [ici](https://jira.aleksya.io/secure/attachment/10200/BullitEngine.xml).